		  &larr; <a href="{$site.url.req}">������</a>
		  <hr />
		  <div class="tenderPayment">{$list.e.oplata}</div>
		  <h1 class="newTenderAdd">
		    ������ �{$list.e.id}
			{if $site.user.group == 1 || $site.user.id == $list.e.author.id}
			{if $site.user.logged}
			<a href="{$list.e.url.edit}" title="������������� ������"><img src="/templates/img/edit.png" alt="������������� ������" /></a>
			<a href="{$list.e.url.delete}" title="������� ������"><img src="/templates/img/delete.png" alt="������� ������" /></a>
			{/if}
			{/if}
		  </h1>
		  <div class="tenders">
		    <div id="tender{$list.e.id}" class="tender tenderPage">
			  <div class="tenderContent">
				<div class="tenderAdress">
				  {if $list.e.from_address}<span class="adr">����� �����������: <span>{$list.e.from_address}</span></span>{/if}
				  {if $list.e.to_address}<span class="adr">����� ��������: <span>{$list.e.to_address}</span></span>{/if}
				  {if $list.e.isMap}<span><a href="javascript://" onclick="{$list.e.toggleMap}" class="jLink">�������� �� �����</a></span>{/if}
				</div>
				{if $list.e.isMap}{$list.e.map}{/if}
				<div class="tenderInfo">
				  {if $list.e.dlina || $list.e.vysota || $list.e.shirina || $list.e.tonnaj || $list.e.help}<div class="tenderInfoGruz">
				    <strong>���������� � �����:</strong>
				    <ul>
					  {if $list.e.dlina}<li>�����: {$list.e.dlina} ������</li>{/if}
					  {if $list.e.vysota}<li>������: {$list.e.vysota} �����</li>{/if}
					  {if $list.e.shirina}<li>������: {$list.e.shirina} ����</li>{/if}
					  {if $list.e.tonnaj}<li>������: {$list.e.tonnaj} �����</li>{/if}
					  {if $list.e.help}<li>���������� ������ �������� � �������� ��������</li>{/if}
					</ul>
				  </div>{/if}
				  {if $list.e.gydrolift || $list.e.kuzov || $list.e.koniki || $list.e.manipulator}<div class="tenderInfoAuto">
				    <strong>���������� � ����������� ���������:</strong>
					<ul>
					  <li>��� ���������: {$list.e.kuzov}</li>
					  {if $list.e.gydrolift}<li>������� ����������</li>{/if}
					  {if $list.e.koniki}<li>������� �������</li>{/if}
					  {if $list.e.manipulator}<li>������� ������������</li>{/if}
					</ul>
				  </div>{/if}
				</div>
				{if $list.e.message}<strong>�������������� ����������:</strong><br />
				{$list.e.message}{/if}
				{if $list.e.performer}
				<div class="tenderReviewed">
				  <div class="tenderReviewedPerformer">{if $list.e.performer == $site.user.id}������������ ������� ������ <a href="{$site.url.user_profile}" class="online">{$site.user.imya}{if $site.user.familiya} {$site.user.familiya}{/if}</a> <sup class="{$site.user.rateClass}">{$site.user.rate}</sup>{else}������������ ������� ������ <a href="{$list.e.performerInfo.url}" class="{$list.e.performerInfo.status}">{$list.e.performerInfo.imya}{if $list.e.performerInfo.familiya} {$list.e.performerInfo.familiya}{/if}</a> <sup class="{$list.e.performerInfo.rateClass}">{$list.e.performerInfo.rate}</sup>{/if}</div>
			      
				  {if $list.e.performer == $site.user.id || $site.user.id == $list.e.uid}
				  <div class="tenderReviewedLinks">
				    {if $list.e.myComment.message}<a href="javascript://" onclick="$('.jLink.active').removeClass('active');$(this).addClass('active');$('#tenderYouReviewed').show();$('#tenderPerformerReviewed, #tenderAddReviewed').hide();" class="jLink {if $list.e.myComment.message}active{/if}">��� �����</a>{/if}
				    {if $list.e.performerComment.message}<a href="javascript://" class="jLink {if !$list.e.myComment.message && $list.e.performerComment.message}active{/if}" onclick="$('.jLink.active').removeClass('active');$(this).addClass('active');$('#tenderPerformerReviewed').show();$('#tenderAddReviewed, #tenderYouReviewed').hide();">����� {if $list.e.performer == $site.user.id}���������{else}�����������{/if}</a>{/if}
				    {if !$list.e.myComment.message}<a href="javascript://" class="jLink {if !$list.e.myComment.message && !$list.e.performerComment.message}active{/if}" onclick="$('.jLink.active').removeClass('active');$(this).addClass('active');$('#tenderAddReviewed').show();$('#tenderPerformerReviewed, #tenderYouReviewed').hide();">�������� �����</a>{/if}
				  </div>
				  <div {if $list.e.myComment.message}style="display:block;" {/if}class="tenderReviewedContent" id="tenderYouReviewed">
					{$list.e.myComment.message}
				  </div>
				  <div {if !$list.e.myComment.message && $list.e.performerComment.message}style="display:block;" {/if}class="tenderReviewedContent" id="tenderPerformerReviewed">
				    {$list.e.performerComment.message}
				  </div>
				  <div {if !$list.e.myComment.message && !$list.e.performerComment.message}style="display:block;" {/if}class="tenderReviewedContent" id="tenderAddReviewed">
				   <form action="/comment.send.php" method="post">
				    <input name="toId" type="hidden" value="{$list.e.id}"/>
				    <input name="uid" type="hidden" value="{$site.user.id}"/>
					<select name="type">
					  <option value="plus">�������������</option>
					  <option value="minus">�������������</option>
					</select><br />
					<textarea name="message"></textarea><br />
					<input type="submit" value="�������� �����" />
				   </form>
				  </div>
				  {/if}
				  
			    </div>
				{/if}
				<div class="tenderAuthor">
				  <a href="{$list.e.author.url}" class="{$list.e.author.status}">{$list.e.author.imya}{if $list.e.author.familiya} {$list.e.author.familiya}{/if}</a> <sup class="{$list.e.author.rateClass}">{$list.e.author.rate}</sup>{if $list.e.author.phone}, {$list.e.author.phone}{/if}
				</div>
				<div class="tenderBottom">
				  <span>������ ������������ {$list.e.date}</span><br />
				  {if $site.user.ban != 1 && $site.user.logged && $list.e.uid != $site.user.id}<a href="#answer">������� �����������</a>{if $list.e.answers > 0} <span>({$list.e.answers})</span>{/if}{/if}
				</div>
			  </div>
			</div>
		  </div>
		{if $list.e.answers > 0}
		  <div class="tenderComments">
                    <a id="answer" name="answer"></a>
			<h3>��{if $list.e.uid == $site.user.id} �����{/if} ������ ��������� {$list.e.answers} ���������{$list.e.answersEnding}</h3>
			{section name=i loop=$list.e.comments}
			<div class="tenderComment{if $smarty.section.i.index%2} bg{/if}">
			 {if $list.e.uid == $site.user.id && $site.user.ban != 1}
			  <div class="tenderCommentPerformer">
				{if $list.e.performer == $list.e.comments[i].author.id}
				  <a href="javascript:requests.check(0, {$list.e.id}, {$list.e.comments[i].author.id});">���� ������� �� ����� ��������� �����</a>
				{elseif !$list.e.performer}
				  <a href="javascript:requests.check(1, {$list.e.id}, {$list.e.comments[i].author.id});">������� �������� ��� �����������</a>
				{/if}
			  </div>
			 {/if}
			  <div class="tenderCommentTop">
			    <a href="{$list.e.comments[i].author.url}" class="{$list.e.comments[i].author.status}">{$list.e.comments[i].author.imya}{if $list.e.comments[i].author.familiya} {$list.e.comments[i].author.familiya}{/if}</a> <sup class="{$list.e.comments[i].author.rateClass}">{$list.e.comments[i].author.rate}</sup> <span>{$list.e.comments[i].author.ulogin}, {$list.e.comments[i].date}</span>
			  </div>
			  <div class="tenderCommentContent">
			    {$list.e.comments[i].message}
			  </div>
				{if $list.e.comments[i].corrCount > 0}
				<div class="tenderCommentBottom">
			    <a href="javascript://" onclick="toggle('comid{$list.e.comments[i].id}', this, 1);" class="jLink">���������� ���������</a>
				</div>
				{else}
					{if $list.e.comments[i].uid == $site.user.id || $list.e.uid == $site.user.id}
					{if $site.user.ban != 1}
					<div class="tenderCommentBottom">
						<a href="javascript://" onclick="$('#comid{$list.e.comments[i].id}').toggle();" class="jLink">��������</a>
					</div>
					{/if}
					{/if}
				{/if}
			  <div class="tenerMessages{if $smarty.section.i.index%2} bg{/if}" id="comid{$list.e.comments[i].id}" style="display:none;">
			  {section name=b loop=$list.e.comments[i].corr}
			    <div class="tenderComment">
			      <div class="tenderCommentTop">
			        <a href="{$list.e.comments[i].corr[b].author.url}" class="{$list.e.comments[i].corr[b].author.status}">{$list.e.comments[i].corr[b].author.imya}{if $list.e.comments[i].corr[b].author.familiya} {$list.e.comments[i].corr[b].author.familiya}{/if}</a> <sup class="{$list.e.comments[i].corr[b].author.rateClass}">{$list.e.comments[i].corr[b].author.rate}</sup> <span>{$list.e.comments[i].corr[b].author.ulogin}, {$list.e.comments[i].corr[b].date}</span>
			      </div>
			      <div class="tenderCommentContent">
			        {$list.e.comments[i].corr[b].message}
			      </div>
			    </div>
			  {/section}
				<div id="tenderCommentId"></div>
				{if $list.e.comments[i].uid == $site.user.id || $list.e.uid == $site.user.id}
				{if $site.user.ban != 1}
                    <form id="corr_form{$list.e.comments[i].id}" onsubmit="if($('#corrMessage{$list.e.comments[i].id}').val().trim().length > 0) $.post('{$list.e.url.answer}&ajax=1&random=' + Math.random(), $(this).serialize(), function{literal}(data){if(data == 100) {$('#comid{/literal}{$list.e.comments[i].id} #corr_form{$list.e.comments[i].id}').before('<div class=\'tenderComment\'><div class=\'tenderCommentTop\'><a class=\'online\' href=\'{$site.user.url}\'>{$site.user.imya}{if $site.user.familiya} {$site.user.familiya}{/if}</a> <sup class=\'{$site.user.rateClass}\'>{$site.user.rate}</sup> <span>{$site.user.ulogin}, ������ ���</span></div><div class=\'tenderCommentContent\'>' + $('#corrMessage{$list.e.comments[i].id}').val() + '</div></div>');$('#corrMessage{$list.e.comments[i].id}').val('');{literal}} else alert('������');}{/literal}); else $('#corrMessage{$list.e.comments[i].id}').focus();return false;" action="{$list.e.url.answer}&random={math equation='rand(10,100)'}" method="post">
				  <input type="hidden" name="toId" value="{$list.e.id}"/>
				  <input type="hidden" name="key" value="{$list.e.key}"/>
				  <input type="hidden" name="corr" value="{$list.e.comments[i].id}">
				  <input type="hidden" name="corr_key" value="{$list.e.comments[i].corr_key}">
				  <div class="fr sfr">
				    <textarea id="corrMessage{$list.e.comments[i].id}" class="frText w50px w100" name="message"></textarea>
				  </div>
				  <div class="frSubmit"><input type="submit" value="��������" /></div>
				</form>
				{/if}
				{/if}
			  </div>
			</div>
			{/section}
		  </div>
		{else}
		  {if $list.e.uid == $site.user.id}
		   <div class="regError">{$site.lang.errors[600]}</div>
		  {/if}
		{/if}
		{if $site.user.ban != 1 && $site.user.logged && $list.e.author.id != $site.user.id && !$list.e.closed}
		  <div class="tenderCommentForm" id="tenderCommentForm">
		    <form id="commentForm" onsubmit="{literal}if($('#commentTextarea').val().length > 0) {{/literal}$.post('{$list.e.url.answer}{literal}&ajax=1&random=' + Math.random(), $(this).serialize(), function(data){if(data == 100) {{/literal}$('#commentForm').before('<div class=\'tenderComment\'><div class=\'tenderCommentTop\'><a href=\'{$site.user.url}\' class=\'online\'>{$site.user.imya}{if $site.user.familiya} {$site.user.familiya}{/if}</a> <sup class=\'{$site.user.rateClass}\'>{$site.user.rate}</sup> <span>{$site.user.ulogin}, ������ ���</span></div><div class=\'tenderCommentContent\'>' + $('#commentTextarea').val() + '</div></div>');{literal}$('#commentTextarea').val('');}})} else $('#commentTextarea').focus();return false;{/literal}" action="{$list.e.url.answer}" method="post">
			<input type="hidden" name="toId" value="{$list.e.id}"/>
			<input type="hidden" name="key" value="{$list.e.key}"/>
			  <div class="fr sfr">
			    <label for="tenderUslug">&mdash; �������� ��� ���� �� ������ ���������� ���� ������ �� ������.</label>
				<textarea id="commentTextarea" class="frText w100 w150px" name="message"></textarea>
			  </div>
			  <div class="frSubmit sfr"><input type="submit" value="���������� ���� ������" /></div>
			</form>
		  </div>
		{/if}
{if $site.user.ban != 1}
<script type="text/javascript">
{literal}
var requests = {
	del: function(id){
		if(confirm('�� ������������� ������ ������� ������?'))
			$.post('/post.php?act=delete&module=requests', {
				id: id,
				uid: {/literal}{$site.user.id}{literal}
			}, function(data){
				if(data == 1) location.href = '/';
				else alert(data);
			});
	},
	check: function(action, id, uid){
		$.post('/requests.check.php', {
			act: action,
			id: id,
			performer: uid,
			uid: {/literal}{$site.user.id}{literal}
		}, function(data){
			if(data == 1) location.reload();
			else alert(data);
		});
	}
};
{/literal}
</script>
{/if}