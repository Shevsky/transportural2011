<div class="forumHk">
<a href="{$site.url.forum}">�����</a> &rarr;
<a href="/forum/{$forum.id}">{$forum.name}</a> &rarr;
{$topic.name}
</div>
{if $list.pages > 1}
<div style="margin: -20px 0 20px;" class="overflow">
<div style="padding:5px 0 4px 0;" class="ForumPageNav">
<span class="pageSumm">�������� {$list.page} �� {$list.pages}:</span>&nbsp;&nbsp;{$list.paginator}
</div>
</div>
{/if}
<div class="forumTopInfo">
<h3><span id="topicNameEdit">{$topic.name}</span><input id="topicNameEditInput" name="name" type="text" style="display:none;" value="{$topic.name}"/> <input id="topicNameEditButton" style="display:none;" value="���������" type="button" onclick="change_topic_name({$topic.id});">{if $topic.uid == $site.user.id || $site.user.group == 1} <a id="topicNameEditLink" href="javascript:edit_topic_name();"><img src="/templates/img/edit.png" alt="�������������" /></a>{/if}</h3>
</div>
{section name=i loop=$posts}
<table class="forumPost">
<tbody><tr>
<td class="forumPostName">
<a class="{$posts[i].author.status}" rel="nofollow" href="/user/{$posts[i].login}">{$posts[i].login}</a>
</td>
<td class="forumPostInfo">
<a rel="nofollow" class="txtSm" href="#msg{$posts[i].id}" name="msg{$posts[i].id}" id="msg{$posts[i].id}">#{$posts[i].num}</a> |
<strong>{$posts[i].date}</strong>, {$posts[i].time}
</td>
</tr>
<tr>
<td{if !$site.user.logged} style="border-bottom:1px solid #E9E9B5;"{/if} class="forumPostAvatar">
<img alt="" src="{$posts[i].author.userpic}">
<br><br>
{$posts[i].author.imya}{if $posts[i].author.familiya} {$posts[i].author.familiya}{/if}
</td>
<td id="forumTextId{$posts[i].id}" class="forumPostMessage">{$posts[i].text}</td>
</tr>
<tr>
{if $site.user.logged}<td class="forumPostPodava"></td>
<td class="forumPostAdmin">
{if $site.user.id != $posts[i].uid}<a onclick="$('#postText').val($('#postText').val() + '{$posts[i].author.imya}{if $posts[i].author.familiya} {$posts[i].author.familiya}{/if}, ').focus();" href="javascript://">��������</a>
<span class="divider">|</span>
<a onclick="tag_quote('postText', '{$posts[i].login}', 'forumTextId{$posts[i].id}');" href="javascript://">����������</a>
{/if}
{if $posts[i].uid == $site.user.id || $site.user.group == 1}
{if $site.user.id != $posts[i].uid}
<span class="divider">|</span>
{/if}
<a href="/forum/edit/message/{$posts[i].id}">�������������</a>
{/if}
{if $site.user.group == 1 && $smarty.section.i.index != 0}
<span class="divider">|</span>
<a href="javascript:delete_message({$posts[i].id}, {$forum.id}, {$topic.id});">�������</a>{/if}
</td>
{/if}
</tr>
</tbody></table>
{/section}
{if $list.pages > 1}
<div class="overflow">
<div style="padding:5px 0 4px 0;" class="ForumPageNav">
<span class="pageSumm">�������� {$list.page} �� {$list.pages}:</span>&nbsp;&nbsp;{$list.paginator}
</div>
</div>
{/if}
<script type="text/javascript">
{literal}
function form_forum_post(){
	if($('#postText').val().length > 0) {
		$('#postMsg').submit();
	} else popup('������', '�� ������ �������� ���������');
};
function edit_topic_name(){
	$('#topicNameEdit, #topicNameEditLink, #topicNameEditInput, #topicNameEditButton').toggle();
};
function change_topic_name(id){
	if($('#topicNameEditInput').val().length > 3)
		$.post('/forum.edit.php?action=topic&id=' + id, {
			name: $('#topicNameEditInput').val(),
			id: id
		}, function(data){
			if(data == 1) {
				$('#topicNameEdit').html($('#topicNameEditInput').val());
				edit_topic_name();
			} else alert('������');
		});
	else popup('������', '����������� ����� �������� � 3 �������');
};
{/literal}
{if $site.user.group == 1}
{literal}
function delete_message(id, forum, topic){
	if(confirm('�� ������������� ������ ������� ��� ���������?')){
		$.post('/forum.delete.php', {
			action: 'message',
			id: id,
			forum: forum,
			topic: topic
		}, function(){
			location.reload();
		});
	};
};
function topic_delete(id, forum){
	if(confirm('�� ������������� ������ ������� ��� ����? ��� ���������, ����������� � ���, ����� �������!'))
		$.post('/forum.delete.php', {
			action: 'topic',
			id: id,
			forum: forum
		}, function(){
			location.href = '/forum/' + forum;
		});
};
function topic_close(id, m){
	$.post('/forum.topic.actions.php', {
		action: 'close',
		id: id,
		m: m
	}, function(){
		location.reload();
	});
};
function topic_stick(id, m){
	$.post('/forum.topic.actions.php', {
		action: 'stick',
		id: id,
		m: m
	}, function(){
		location.reload();
	});
};
{/literal}
{/if}
</script>
{if $site.user.logged && !$topic.closed}
<div id="newtopic" class="addtopic">
<div class="addtopicLink">
<h2>����� ���������</h2>
</div>
<form class="formStyle" method="post" action="/forum.posts.post.php" id="postMsg">
<input type="hidden" name="last_post_num" value="{$last_post_num}">
<input type="hidden" name="topic" value="{$topic.id}">
<input type="hidden" name="forum" value="{$forum.id}">
<div class="fr forum">
<a href="javascript://" onclick="simpletag('b','','','postText','');"><img title="������" alt="������" src="/templates/img/bb/b.gif"></a>&nbsp;

<a href="javascript://" onclick="simpletag('i','','','postText','');"><img title="������" alt="������" src="/templates/img/bb/i.gif"></a>&nbsp;

<a href="javascript://" onclick="tag_image('postText');"><img title="������ �� ��������" alt="������ �� ��������" src="/templates/img/bb/img.gif"></a>&nbsp;

<a href="javascript://" onclick="tag_video('postText');"><img title="������ �� �����������" alt="������ �� �����������" src="/templates/img/bb/video.gif"></a>&nbsp;

<a href="javascript://" onclick="tag_url('postText');"><img title="������� ������" alt="������� ������" src="/templates/img/bb/http.gif"></a>
<textarea tabindex="2" class="frText w100" rows="10" cols="38" id="postText" name="text"></textarea>
</div>
<div class="fr forum submit">
<div id="buttonSubmitJavascript"></div>
<script type="text/javascript">$('#buttonSubmitJavascript').html('<input type="button" onclick="form_forum_post();" tabindex="5" class="inputButton" value="��������� ���������" name="subbut">');</script>
</div>
</form>
</div>
{/if}
{if $topic.closed && $site.user.logged}
<div class="topicClose">��� ���� �������. ������ ������ �� �����������.</div>
{/if}
{if $site.user.group == 1}
<div class="topicAdmin">
<a href="javascript:topic_close({$topic.id}, {if $topic.closed}0{else}1{/if});">{if $topic.closed}�������{else}�������{/if} ����</a>
<span class="divider">|</span>
<a href="javascript:topic_delete({$topic.id}, {$forum.id});">������� ����</a>
<span class="divider">|</span>
<a href="javascript:topic_stick({$topic.id}, {if $topic.stick}0{else}1{/if});">{if $topic.stick}��������� ����{else}��������� ����{/if}</a>  
</div>
{/if}
{if !$site.user.logged}
<div align="center" style="margin:20px 0;">
�� �� ������ ������ � ������, ��� ��� �� ������������. <a href="{$site.url.login}">�������</a> ��� <a href="{$site.url.signup}">�����������������</a>,<br>����� ���� ��� ����� ���� ����������� ������������ ����� � ������ ������.
</div>
{/if}