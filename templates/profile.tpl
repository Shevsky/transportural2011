		{if $site.user.group == 1 && $profile.group != 1}
		  <div class="userBan">
			<div class="userBanLink">
				<a href="javascript:user.{if $profile.ban == 1}un{/if}ban({$profile.id});">{if $profile.ban == 1}��������������{else}�������������{/if}</a>
			</div>
		 {if $profile.ban == 1}
			<div class="userBanTxt">
				���������� ������������ ������������� �� {$profile.ban_date} ����.
			</div>
		 {/if}
		  </div>
		{/if}
		  <div class="userTop">
		    {if $profile.userpic}<div class="userPic">
		      <img style="max-width:100px;max-height:100px;" src="{$profile.userpic}" alt="" />
		    </div>{else}<div class="userPic">
		      <img style="max-width:100px;max-height:100px;" src="/templates/img/userpic.png" alt="" />
		    </div>{/if}
		    <div class="userLinks">
		      <strong><a href="javascript://" class="{$profile.status}">{$profile.imya}{if $profile.otchestvo} {$profile.otchestvo}{/if}{if $profile.familiya} {$profile.familiya}{/if}</a></strong><br />
			  {if $profile.id == $site.user.id}
		      <a href="{$profile.url.edit}">������������� �������</a><br />
			  {else}
			  {if $site.user.group == 1}
			  <a href="{$profile.url.edit}">������������� �������</a><br />
			  {/if}
			  {if $site.user.logged && $site.user.ban != 1}
			  <a href="/mail/{$profile.ulogin}">�������� ���������</a><br />
			  {/if}
			  {/if}
		      ���������: <span class="{$profile.rateClass}">{$profile.rate}</span><br />
		      ������� ����������������: <span class="{$profile.freighter_reputationClass}">{$profile.freighter_reputation}</span><br />
		      ������� ��������������: <span class="{$profile.customer_reputationClass}">{$profile.customer_reputation}</span><br />
		      {if $profile.type_faces > 0}���: {$site.lang.type_faces[$profile.type_faces-1]}{if $profile.type_faces == 3}<br />
			  ��������: {$profile.company_name}
			  {/if}{/if}
		    </div>
			<div class="userInfo">
			  <ul>
			    {if $profile.birthday}<li>���� ��������: {$profile.birthday}</li>{/if}
				{if $profile.sex}<li>���: {$profile.sex}</li>{/if}
			    {if $profile.phone}<li>�������: {$profile.phone}</li>{/if}
			    <li>����������� �����: <a href="mailto:{$profile.email}">{$profile.email}</a></li>
			  </ul>
			</div>
		  </div>
		  {if $profile.active}
		  <div class="profileNav">
			<ul>
			  {if $profile.signature}<li>{if $profile.active != 'information'}<a href="{$profile.url.information}">{/if}����������{if $profile.active != 'information'}</a>{/if}</li>{/if}
			  {if $profile.cargoCount > 0}<li>{if $profile.active != 'auto'}<a href="{$profile.url.auto}">{/if}���������{if $profile.active != 'auto'}</a>{/if}</li>{/if}
			  {if $profile.requestsCount > 0}<li>{if $profile.active != 'tenders'}<a href="{$profile.url.tenders}">{/if}������{if $profile.active != 'tenders'}</a>{/if}</li>{/if}
			  {if $profile.boardCount > 0}<li>{if $profile.active != 'board'}<a href="{$profile.url.board}">{/if}����������{if $profile.active != 'board'}</a>{/if}</li>{/if}
			  {if $profile.reviewsCount > 0}<li>{if $profile.active != 'reviews'}<a href="{$profile.url.reviews}">{/if}������{if $profile.active != 'reviews'}</a>{/if}</li>{/if}
			</ul>
		  </div>
		  <div class="profileContent">
		    {if $profile.signature}
				{if $profile.active == 'information'}{$profile.signature}{/if}
			{/if}
			{if $profile.cargoCount > 0}
				{if $profile.active == 'auto'}{include file='cargo.entry.tpl' list=$profile.cargo}{/if}
			{/if}
			{if $profile.requestsCount > 0}
				{if $profile.active == 'tenders'}{include file='requests.entry.tpl' list=$profile.requests}{/if}
			{/if}
			{if $profile.boardCount > 0}
				{if $profile.active == 'board'}{include file='board.entry.tpl' list=$profile.board}{/if}
			{/if}
			{if $profile.reviewsCount > 0}
			  {if $profile.active == 'reviews'}
			  {section loop=$profile.reviews name=i}
			  <div class="otz{if $smarty.section.i.index%2} bg{/if}">
			    <div class="otzTop">
				  <div class="otzRep"><span class="otz{if $profile.reviews[i].type == 'minus'}Down{else}Up{/if}">{if $profile.reviews[i].type == 'minus'}�������������{else}�������������{/if}</span></div>
				  <a href="{$profile.reviews[i].author.url}" class="{$profile.reviews[i].author.status}">{$profile.reviews[i].author.imya}{if $profile.reviews[i].author.familiya} {$profile.reviews[i].author.familiya}{/if}</a> <sup class="{$profile.reviews[i].author.rateClass}">{$profile.reviews[i].author.rate}</sup> <span>{$profile.reviews[i].date}</span>
				</div>
				<div class="otzText">
				  {$profile.reviews[i].message}
				</div>
			  </div>
			  {/section}
			  {/if}
			{/if}
		  </div>
		  {/if}

<script type="text/javascript">
var site_user_id = {$site.user.id};
{literal}
var requests = {
	del: function (b) {
		confirm("\u0412\u044b \u0434\u0435\u0439\u0441\u0442\u0432\u0438\u0442\u0435\u043b\u044c\u043d\u043e \u0445\u043e\u0442\u0438\u0442\u0435 \u0443\u0434\u0430\u043b\u0438\u0442\u044c \u0437\u0430\u044f\u0432\u043a\u0443?") && $.post("/post.php?act=delete&module=requests", {
			id: b,
			uid: site_user_id
		},
		function (a) {
			a == 1 ? $("#tender" + b).hide() : alert(a)
		})
	},
	oc: function (b, a) {
		confirm("\u0412\u044b \u0434\u0435\u0439\u0441\u0442\u0432\u0438\u0442\u0435\u043b\u044c\u043d\u043e \u0445\u043e\u0442\u0438\u0442\u0435 " + (a ? "\u0437\u0430\u043a\u0440\u044b\u0442\u044c": "\u043e\u0442\u043a\u0440\u044b\u0442\u044c") + " \u0437\u0430\u044f\u0432\u043a\u0443?") && $.post("/open-close.php", {
			id: b,
			uid: site_user_id,
			act: a
		},
		function (a) {
			a == 1 ? location.reload() : alert(a)
		})
	}
};
var cargo = {
	del: function (b) {
		confirm("\u0412\u044b \u0434\u0435\u0439\u0441\u0442\u0432\u0438\u0442\u0435\u043b\u044c\u043d\u043e \u0445\u043e\u0442\u0438\u0442\u0435 \u0443\u0434\u0430\u043b\u0438\u0442\u044c \u0433\u0440\u0443\u0437\u043e\u0432\u0438\u043a?") && $.post("/post.php?act=delete&module=cargo", {
			id: b,
			uid: site_user_id
		},
		function (a) {
			a == 1 ? $("#entry" + b).hide() : alert(a)
		})
	}
};
var board = {
	del: function (b) {
		confirm("\u0412\u044b \u0434\u0435\u0439\u0441\u0442\u0432\u0438\u0442\u0435\u043b\u044c\u043d\u043e \u0445\u043e\u0442\u0438\u0442\u0435 \u0443\u0434\u0430\u043b\u0438\u0442\u044c \u044d\u0442\u043e \u043e\u0431\u044a\u044f\u0432\u043b\u0435\u043d\u0438\u0435?") && $.post("/post.php?act=delete&module=board", {
			id: b,
			uid: site_user_id
		},
		function (a) {
			a == 1 ? $("#board" + b).hide() : alert(a)
		})
	}
};
{/literal}
{if $site.user.group == 1}
{literal}
var user = {
	ban: function(id){
		var ban_date = prompt('�� ������� ���� ������������� ������������?');
		if(ban_date) $.post('/admin/users.php?act=ban&method=post', {
			id: id,
			days: ban_date
		}, function(data){
				location.reload();
		});
	},
	unban: function(id){
		$.post('/admin/users.php?act=unban&method=post', {
			id: id
		}, function(data){
				location.reload();
		});
	}
};
{/literal}
{/if}
</script>
<script type="text/javascript" src="/js/iload/iLoad.js"></script>