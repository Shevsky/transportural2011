<title>{$title}</title> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="StyleSheet" type="text/css" href="/templates/style.css?1" />
<link rel="stylesheet" href="/js/codemirror/lib/codemirror.css"> 
<script src="/js/codemirror/lib/codemirror.js"></script> 
<script src="/js/nohtml.js"></script> 
<link rel="stylesheet" href="/js/codemirror/theme/default.css"> 
<script src="/js/codemirror/mode/xml/xml.js"></script>
<script src="/js/bbcodes.js"></script>
<script src="/js/jquery.min.js"></script>
<script src="/js/hidemap.js"></script>
<script type="text/javascript" src="/js/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="/js/isRequired.js"></script>
<script type="text/javascript" src="/js/search.js"></script>
<script type="text/javascript" src="http://www.modernizr.com/downloads/modernizr-2.0.6.js"></script>
<link href="/templates/img/favicon.ico" rel="shortcut icon" type="image/x-icon" /> 
 <script type="text/javascript">
{literal}
function submitForm(){
	if($('.codepress').length) {
			eval($('.codepress').attr('id').slice(0, -3) + '.toggleEditor();');
			return true;
	} else return false;
};
{/literal}
</script>
<style type="text/css">
.divider {
	color: #ccc;
	margin: 0 3px;
}
</style>