<script src="http://api-maps.yandex.ru/1.1/index.xml?key={$site.lang.ymaps_key}" type="text/javascript"></script>
<script type="text/javascript">
{literal}
cargo = {
	toggleMap: function (d, b, e) {
		YMaps.jQuery(function () {
			var c = new YMaps.Map(YMaps.jQuery("#YMapsCargo" + d)[0]);
			c.setCenter(new YMaps.GeoPoint(b[0], b[1]), 13);
			var a = new YMaps.Placemark(new YMaps.GeoPoint(b[0], b[1]));
			a.description = e;
			c.addOverlay(a);
			a.setIconContent("\u041c\u0435\u0441\u0442\u043e \u043d\u0430\u0445\u043e\u0436\u0434\u0435\u043d\u0438\u044f");
			a = new YMaps.Zoom({
				customTips: [{
					index: 1,
					value: "\u041c\u0435\u043b\u043a\u043e"
				},
				{
					index: 9,
					value: "\u0421\u0440\u0435\u0434\u043d\u0435"
				},
				{
					index: 16,
					value: "\u041a\u0440\u0443\u043f\u043d\u043e"
				}]
			});
			c.addControl(a)
		})
	}
};
{/literal}
</script>
{section name=i loop=$list.entries}
		    <div id="entry{$list.entries[i].id}" class="truck{if $smarty.section.i.index%2} bg{/if}">
			  <div class="truckContent">
				<div class="truckPanel">
			{if $site.user.group == 1 || $site.user.id == $list.entries[i].author.id}
			{if $site.user.logged}
			<a href="{$list.entries[i].url.edit}" title="������������� ������"><img src="/templates/img/edit.png" alt="������������� ������" /></a>
			<a href="{$list.entries[i].url.delete}" title="������� ������"><img src="/templates/img/delete.png" alt="������� ������" /></a>
			{/if}
			{/if}
				</div>
				<div class="truckAuthor">
				  <a href="{$list.entries[i].author.url}" class="{$list.entries[i].author.status}">{$list.entries[i].author.imya}{if $list.entries[i].author.familiya} {$list.entries[i].author.familiya}{/if}</a> <sup class="{$list.entries[i].author.rateClass}">{$list.entries[i].author.rate}</sup>{if $list.entries[i].author.phone}, {$list.entries[i].author.phone}{/if}
				</div>
				<div class="truckAdress">
				  <span class="adr">�����: <span>{$list.entries[i].marka}</span></span>
				  {if $list.entries[i].rajon}<span class="adr">����� ����������: <span>{$list.entries[i].rajon}</span></span>
				  {if $list.entries[i].isMap}<span><a href="javascript://" onclick="{$list.entries[i].toggleMap}" class="jLink">�������� �� �����</a></span>{/if}{/if}
				  {if $list.entries[i].photo[0]}<span><a href="{$list.entries[i].photo[0]}" rel="iLoad" class="jLink">���������� ����</a></span>{/if}
				</div>
				{if $list.entries[i].isMap}{$list.entries[i].map}{/if}
				<div class="truckAdress">
				  {if $list.entries[i].price_from_hour && $list.entries[i].price_to_hour}<span>�� {$list.entries[i].price_from_hour} �� {$list.entries[i].price_to_hour} ������ �� ���</span>{/if}
				  {if $list.entries[i].price_from_km && $list.entries[i].price_to_km}<span>�� {$list.entries[i].price_from_km} �� {$list.entries[i].price_to_km} ������ �� ��������</span>{/if}
				  <span><a href="{$list.entries[i].url.view}">������ �� ��������</a></span>
				</div>
				<div class="truckInfo">
				  {if $list.entries[i].kuzov || $list.entries[i].gruzopod || $list.entries[i].dlina || $list.entries[i].vysota || $list.entries[i].shirina}<div class="truckInfoGruz">
				    <strong>���������� � ���������:</strong>
				    <ul>
					  {if $list.entries[i].kuzov}<li>��� ���������: {$list.entries[i].kuzov}</li>{/if}
					  {if $list.entries[i].gruzopod}<li>����������������: {$list.entries[i].gruzopod} �����</li>{/if}
					  {if $list.entries[i].dlina}<li>�����: {$list.entries[i].dlina} ������</li>{/if}
					  {if $list.entries[i].vysota}<li>������: {$list.entries[i].vysota} �����</li>{/if}
					  {if $list.entries[i].shirina}<li>������: {$list.entries[i].shirina} ����</li>{/if}
					</ul>
				  </div>{/if}
				  <div class="truckInfoAuto">
				    <br />
					<ul>
					  {if $list.entries[i].pogruzka}<li>��������: {$list.entries[i].pogruzka}</li>{/if}
					  {if $list.entries[i].gydrolift}<li>������� ����������</li>{/if}
					  {if $list.entries[i].koniki}<li>������� �������</li>{/if}
					  {if $list.entries[i].manipulator}<li>������� ������������</li>{/if}
					  {if $list.entries[i].help}<li>�������� ������ �������� � �������� ��������</li>{/if}
					</ul>
				  </div>
				</div>
			  </div>
			</div>
{sectionelse}
<div class="error">
���������� ���
</div>
{/section}