<script src="http://api-maps.yandex.ru/1.1/index.xml?key={$site.lang.ymaps_key}" type="text/javascript"></script>
<script type="text/javascript">
{literal}
cargo = {
	toggleMap: function (d, b, e) {
		YMaps.jQuery(function () {
			var c = new YMaps.Map(YMaps.jQuery("#YMapsCargo" + d)[0]);
			c.setCenter(new YMaps.GeoPoint(b[0], b[1]), 13);
			var a = new YMaps.Placemark(new YMaps.GeoPoint(b[0], b[1]));
			a.description = e;
			c.addOverlay(a);
			a.setIconContent("\u041c\u0435\u0441\u0442\u043e \u043d\u0430\u0445\u043e\u0436\u0434\u0435\u043d\u0438\u044f");
			a = new YMaps.Zoom({
				customTips: [{
					index: 1,
					value: "\u041c\u0435\u043b\u043a\u043e"
				},
				{
					index: 9,
					value: "\u0421\u0440\u0435\u0434\u043d\u0435"
				},
				{
					index: 16,
					value: "\u041a\u0440\u0443\u043f\u043d\u043e"
				}]
			});
			c.addControl(a)
		})
	}
};
{/literal}
</script>
		  &larr; <a href="{$site.url.cargo}">������� �����������������</a>
		  <hr />
<div class="trucks">
		    <div id="entry{$list.e.id}" class="truck truckPage">
			  <div class="truckContent">
				<div class="truckPanel">
			{if $site.user.group == 1 || $site.user.id == $list.e.author.id}
			{if $site.user.logged}
			<a href="{$list.e.url.edit}" title="������������� ������"><img src="/templates/img/edit.png" alt="������������� ������" /></a>
			<a href="{$list.e.url.delete}" title="������� ������"><img src="/templates/img/delete.png" alt="������� ������" /></a>
			{/if}
			{/if}
				</div>
				<div class="truckAuthor">
				  <a href="{$list.e.author.url}" class="{$list.e.author.status}">{$list.e.author.imya}{if $list.e.author.familiya} {$list.e.author.familiya}{/if}</a> <sup class="{$list.e.author.rateClass}">{$list.e.author.rate}</sup>{if $list.e.author.phone}, {$list.e.author.phone}{/if}
				</div>
				<div class="truckAdress">
				  {if $list.e.marka}<span>�����: {$list.e.marka}</span>{/if}
				  {if $list.e.rajon}<span>����� ����������: {$list.e.rajon}</span>
				  {if $list.e.isMap}<span><a href="javascript://" onclick="{$list.e.toggleMap}" class="jLink">�������� �� �����</a></span>{/if}{/if}
				  {if $list.e.photo[0]}<span><a href="{$list.e.photo[0]}" rel="iLoad" class="jLink">���������� ����</a></span>{/if}
				</div>
				{if $list.e.isMap}{$list.e.map}{/if}
				<div class="truckAdress">
				  {if $list.e.price_from_hour && $list.e.price_to_hour}<span>�� {$list.e.price_from_hour} �� {$list.e.price_to_hour} ������ �� ���</span>{/if}
				  {if $list.e.price_from_km && $list.e.price_to_km}<span>�� {$list.e.price_from_km} �� {$list.e.price_to_km} ������ �� ��������</span>{/if}
				</div>
				<div class="truckInfo">
				  <div class="truckInfoGruz">
				    <strong>���������� � ���������:</strong>
				    <ul>
					  {if $list.e.kuzov}<li>��� ���������: {$list.e.kuzov}</li>{/if}
					  {if $list.e.gruzopod}<li>����������������: {$list.e.gruzopod} �����</li>{/if}
					  {if $list.e.dlina}<li>�����: {$list.e.dlina} ������</li>{/if}
					  {if $list.e.vysota}<li>������: {$list.e.vysota} �����</li>{/if}
					  {if $list.e.shirina}<li>������: {$list.e.shirina} ����</li>{/if}
					</ul>
				  </div>
				  <div class="truckInfoAuto">
				    <br />
					<ul>
					  {if $list.e.pogruzka}<li>��������: {$list.e.pogruzka}</li>{/if}
					  {if $list.e.gydrolift}<li>������� ����������</li>{/if}
					  {if $list.e.koniki}<li>������� �������</li>{/if}
					  {if $list.e.manipulator}<li>������� ������������</li>{/if}
					  {if $list.e.help}<li>�������� ������ �������� � �������� ��������</li>{/if}
					</ul>
				  </div>
				</div>
			  </div>
			</div>
</div>
<script type="text/javascript">
{literal}
cargo.del = function(id){
		if(confirm('�� ������������� ������ ������� ��������?'))
			$.post('/post.php?act=delete&module=cargo', {
				id: id,
				uid: {/literal}{$site.user.id}{literal}
			}, function(data){
				if(data == 1) location.href = '/auto';
				else alert(data);
			});
};
{/literal}
</script>
<script type="text/javascript" src="/js/iload/iLoad.js"></script>