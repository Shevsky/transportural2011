<script src="http://api-maps.yandex.ru/1.1/index.xml?key={$site.lang.ymaps_key}" type="text/javascript"></script>
<script type="text/javascript">
{literal}
req = {
	toggleMap: function (e, a, b, f, g) {
		YMaps.jQuery(function () {
			var d = new YMaps.Map(YMaps.jQuery("#YMaps" + e)[0]);
			d.setCenter(new YMaps.GeoPoint(a[0] / 2 + b[0] / 2, a[1] / 2 + b[1] / 2), Math.abs(a[0] - b[0]) > 0.1 || Math.abs(a[1] - b[1]) > 0.1 ? 10 : Math.abs(a[0] - b[0]) > 0.04 || Math.abs(a[1] - b[1]) > 0.04 ? 11 : Math.abs(a[0] - b[0]) > 0.02 || Math.abs(a[1] - b[1]) > 0.02 ? 12 : 13);
			var c = new YMaps.Placemark(new YMaps.GeoPoint(a[0], a[1]));
			c.description = f;
			d.addOverlay(c);
			c.setIconContent("\u041f\u0443\u043d\u043a\u0442 \u043e\u0442\u043f\u0440\u0430\u0432\u043b\u0435\u043d\u0438\u044f");
			c = new YMaps.Placemark(new YMaps.GeoPoint(b[0], b[1]));
			c.description = g;
			d.addOverlay(c);
			c.setIconContent("\u041f\u0443\u043d\u043a\u0442 \u043f\u0440\u0438\u0431\u044b\u0442\u0438\u044f");
			c = new YMaps.Zoom({
				customTips: [{
					index: 1,
					value: "\u041c\u0435\u043b\u043a\u043e"
				},
				{
					index: 9,
					value: "\u0421\u0440\u0435\u0434\u043d\u0435"
				},
				{
					index: 16,
					value: "\u041a\u0440\u0443\u043f\u043d\u043e"
				}]
			});
			d.addControl(c)
		})
	}
};
{/literal}
</script>
{section name=i loop=$list.entries}
<div id="tender{$list.entries[i].id}" class="tender{if $smarty.section.i.index%2} bg{/if}">
			  <div class="tenderPayment">{$list.entries[i].oplata}</div>
			  <div class="tenderTitle"><a href="{$list.entries[i].url.view}">������ �{$list.entries[i].id}</a></div>
			  <div class="tenderContent">
				<div class="tenderAdress">
				  {if $list.entries[i].from_address}<span class="adr">����� �����������: <span>{$list.entries[i].from_address}</span></span>{/if}
				  {if $list.entries[i].to_address}<span class="adr">����� ��������: <span>{$list.entries[i].to_address}</span></span>{/if}
				 {if $list.entries[i].isMap}<span><a href="javascript://" onclick="{$list.entries[i].toggleMap}" class="jLink">�������� �� �����</a></span>{/if}
				</div>
				{if $list.entries[i].isMap}{$list.entries[i].map}{/if}
				<div class="tenderInfo">
				  {if $list.entries[i].dlina || $list.entries[i].vysota || $list.entries[i].shirina || $list.entries[i].tonnaj || $list.entries[i].help}<div class="tenderInfoGruz">
				    <strong>���������� � �����:</strong>
				    <ul>
					  {if $list.entries[i].dlina}<li>�����: {$list.entries[i].dlina} ������</li>{/if}
					  {if $list.entries[i].vysota}<li>������: {$list.entries[i].vysota} �����</li>{/if}
					  {if $list.entries[i].shirina}<li>������: {$list.entries[i].shirina} ����</li>{/if}
					  {if $list.entries[i].tonnaj}<li>������: {$list.entries[i].tonnaj} �����</li>{/if}
                      {if $list.entries[i].help}<li>���������� ������ �������� � �������� ��������</li>{/if}
					</ul>
				  </div>{/if}
				  {if $list.entries[i].gydrolift || $list.entries[i].kuzov || $list.entries[i].koniki || $list.entries[i].manipulator}<div class="tenderInfoAuto">
				    <strong>���������� � ����������� ���������:</strong>
					<ul>
					  <li>��� ���������: {$list.entries[i].kuzov}</li>
					  {if $list.entries[i].gydrolift}<li>������� ����������</li>{/if}
					  {if $list.entries[i].koniki}<li>������� �������</li>{/if}
					  {if $list.entries[i].manipulator}<li>������� ������������</li>{/if}
					</ul>
				  </div>{/if}
				</div>
				{if $list.entries[i].message}<strong>�������������� ����������:</strong><br />
				{$list.entries[i].message}{/if}
				{if $list.entries[i].performer}
				<div class="tenderReviewed">
				  <div class="tenderReviewedPerformer">{if $list.entries[i].performer == $site.user.id}������������ ������� ������ <a href="{$site.url.user_profile}" class="online">{$site.user.imya}{if $site.user.familiya} {$site.user.familiya}{/if}</a> <sup class="{$site.user.rateClass}">{$site.user.rate}</sup>{else}������������ ������� ������ <a href="{$list.entries[i].performerInfo.url}" class="{$list.entries[i].performerInfo.status}">{$list.entries[i].performerInfo.imya}{if $list.entries[i].performerInfo.familiya} {$list.entries[i].performerInfo.familiya}{/if}</a> <sup class="{$list.entries[i].performerInfo.rateClass}">{$list.entries[i].performerInfo.rate}</sup>{/if}</div>
				  
				  
			    </div>
				{/if}
				<div class="tenderAuthor">
				  <a href="{$list.entries[i].author.url}" class="{$list.entries[i].author.status}">{$list.entries[i].author.imya}{if $list.entries[i].author.familiya} {$list.entries[i].author.familiya}{/if}</a> <sup class="{$list.entries[i].author.rateClass}">{$list.entries[i].author.rate}</sup>{if $list.entries[i].author.phone}, {$list.entries[i].author.phone}{/if}
				</div>
				<div class="tenderBottom">
				  <span>������ ������������ {$list.entries[i].date}</span><br />
				  {if $site.user.ban != 1 && $site.user.logged && $list.entries[i].uid != $site.user.id && !$list.entries[i].closed}<a href="{$list.entries[i].url.view}#answer">������� �����������</a>{if $list.entries[i].answers > 0} <span>({$list.entries[i].answers})</span>{/if}{/if}
				  {if $list.entries[i].uid == $site.user.id}
					<a href="{$list.entries[i].url.view}">�����������</a>{if $list.entries[i].answers > 0} ({$list.entries[i].answers}){/if}
				  {/if}
				</div>
			{if $site.user.group == 1 || $site.user.id == $list.entries[i].author.id}
			{if $site.user.logged}
				<div class="newPostModer">
				  <a href="{$list.entries[i].url.edit}">�������������</a>
				  {if $site.user.id == $list.entries[i].author.id}{if $list.entries[i].closed}<a href="{$list.entries[i].url.open}">������� ������</a>{else}<a href="{$list.entries[i].url.close}">������� ������</a>{/if}{if $site.user.group == 1}
				  <a href="{$list.entries[i].url.delete}">�������</a>{/if}{else}<a href="{$list.entries[i].url.delete}">�������</a>{/if}
				</div>
			{/if}
			{/if}
			  </div>
			</div>
{sectionelse}
<div class="error">
������ ���
</div>
{/section}