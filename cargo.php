<?php
$script_content = 1;
$script = 'cargo';
$script_uri = 'cargo';

include('params.php');

$nums = $site['options']['show_on_page_cargo'];
if(isset($_GET['page'])) {
	$page = intval($_GET['page']);
} else {
	$page = 1;
};

$sql = mysql_query("SELECT COUNT(*) AS `counter` FROM `cargo`");
$row = mysql_fetch_assoc($sql);
$elements = $row['counter'];

$pages = ceil($elements/$nums);
if($page < 1) {
	$page = 1;
} elseif($page > $pages) {
	$page = $pages;
};
$start = ($page-1) * $nums;
if ($start < 0) $start = 0;
$sql = mysql_query("SELECT * FROM `cargo` ORDER BY id DESC LIMIT {$start}, {$nums}");
$entries = array();
$paginator;
while($row = mysql_fetch_assoc($sql)) {
	$row['url'] = array(
		'edit' => '/auto/edit/' . $row['id'],
		'view' => '/auto/' . $row['id'],
		'delete' => 'javascript:cargo.del(' . $row['id'] . ');',
	);
	$usql = mysql_query("SELECT * FROM `users` WHERE id = " . $row['uid'] . " LIMIT 0, 1");
	$urow = mysql_fetch_assoc($usql);
	$row['author'] = array(
		'familiya' => $urow['familiya'],
		'id' => $urow['id'],
		'imya' => $urow['imya'],
		'otchestvo' => $urow['otchestvo'],
		'birthday' => $urow['birthday'],
		'signature' => $urow['signature'],
		'phone' => ((!$_SESSION['logged'] && $urow['phone_only_reg']) ? false : $urow['phone']),
		'rate' => $urow['rate'],
		'rateClass' => $urow['rate'] > 0 ? 'ratingUp' : ($urow['rate'] < 0 ? 'ratingDown' : ''),
		'active' => $urow['active'],
		'url' => '/user/' . $urow['ulogin'],
		'status' => (strtolower($urow['ulogin']) == $_SESSION['ulogin'] ? 'online' : (time() - $urow['last_entry'] * 1 <= 600 ? 'online': 'offline'))
	);
	$map_geo_from = file_get_contents('http://geocode-maps.yandex.ru/1.x/?format=json&ll=59.147028,57.765608&spn=5.581054,3.082691&rspn=1&geocode=' . urlencode($row['rajon']) . '&key=' . $site['options']['ymaps_key']);
	$map_geo_from = json_decode($map_geo_from, true);
	$geo_from = explode(' ', $map_geo_from['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos']);
	$geo_ad_from = iconv('utf-8','windows-1251',$map_geo_from['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['text']);
	if($geo_from[0] != '60.597340' && $geo_from[1] != '56.837982' && ($geo_from[0] > 59.147028 && $geo_from[0] < 64.728082) && ($geo_from[1] < 57.765608 && $geo_from[1] > 54.682917)) {
		$row['map'] = '<div style="display:none;" class="tenderMap" id="truckmap'.$row['id'].'"><div style="width:600px;height:400px;" id="YMapsCargo'.$row['id'].'"></div></div>';
		$row['isMap'] = true;
		$row['toggleMap'] = 'toggle(\'truckmap'.$row['id'].'\', this);cargo.toggleMap('.$row['id'].', ['.$geo_from[0].', '.$geo_from[1].'], \''.$geo_ad_from.'\');';
	} else $row['isMap'] = false;
		if($row['photo1']) {
			$row['photo'] = array(
				 '/upload/photo-' . $row['id'] . '-1.jpg',
				 '/upload/r/photo-' . $row['id'] . '-1.jpg'
			);
		};
	$entries[] = $row;
};
$neighbours = 3;
$left_neighbour = $page - $neighbours;
if ($left_neighbour < 1) $left_neighbour = 1;
$right_neighbour = $page + $neighbours;
if ($right_neighbour > $pages) $right_neighbour = $pages;
if ($page > 1) {
	$paginator .= '<a href="/auto/page/' . ($page-1) . '">&larr; �����</a>';
};
for ($i=$left_neighbour; $i<=$right_neighbour; $i++) {
	if ($i != $page) {
		$paginator .= '<a href="/auto/page/' . $i . '">' . $i . '</a>';
	} else {
		$paginator .= '<strong>' . $i . '</strong>';
	};
};
if ($page < $pages) {
	$paginator .= '<a href="/auto/page/' . ($page+1) . '">������ &rarr;</a>';
}

$smarty -> assign('list', array(
	'pages' => $pages,
	'entries' => $entries,
	'paginator' => $paginator,
	'action' => $_GET['act'],
	'url' => array(
		'add' => array(
			'cargo' => '/auto/add'
		)
	)
));

$smarty -> assign('title', $site['options']['static_pages']['catalog'] . ' - ' . $site['name']);

$smarty -> display('static.tpl');
?>