<?php
$script_content = 1;
$script = 'search';
$script_uri = 'search';

include('params.php');
setlocale(LC_ALL, 'ru_RU.CP1251', 'rus_RUS.CP1251', 'Russian_Russia.1251');
$_GET['_query'] = iconv('utf-8','windows-1251',$_GET['query']);
$_GET['query'] = iconv('utf-8','windows-1251',mb_strtolower($_GET['query']));

$nums = 10;
if(isset($_GET['npage'])) {
	$page = intval($_GET['npage']);
} else {
	$page = 1;
};

if($_GET['from'] == 'auto')
	$sqlAuto = "SELECT COUNT(*) AS `counter` FROM `cargo` " . ($_GET['kuzov'] != 0 ? ("WHERE `kuzov` = '" . $site['options']['kuzov'][$_GET['kuzov'] * 1 - 1]. "'" . ($_GET['gruzopod'] != 0 ? " and `gruzopod` = '" . $site['options']['gruzopod'][$_GET['gruzopod'] * 1 - 1] . "'" : "")) : ($_GET['gruzopod'] != 0 ? "WHERE `gruzopod` = '" . $site['options']['gruzopod'][$_GET['gruzopod'] * 1 - 1] . "'" : "WHERE 1")) . ($_GET['manipulator'] ? " and `manipulator` = '1'" : "") . ($_GET['gydrolift'] ? " and `gydrolift` = '1'" : "") . ($_GET['koniki'] ? " and `koniki` = '1'" : "") . ($_GET['help'] ? " and `help` = '1'" : "");
if($_GET['from'] != 'all') {
	if($_GET['from'] == 'auto') $sql = mysql_query($sqlAuto);
	elseif($_GET['from'] == 'tenders') $sql = mysql_query($_GET['kuzov'] != 0 ? ("SELECT COUNT(*) AS `counter` FROM `requests` WHERE `kuzov` = '" . $site['options']['kuzov'][$_GET['kuzov'] * 1 - 1] . "'" . ($_GET['closed'] ? " and `closed` = '0'" : "") . " ORDER BY id DESC") : ("SELECT * FROM `requests` " . ($_GET['closed'] ? "WHERE `closed` = '0'" : "") . " ORDER BY id DESC"));
	$row = mysql_fetch_assoc($sql);
	$elements = $row['counter'];

	$pages = ceil($elements/$nums);
	if($page < 1) {
		$page = 1;
	} elseif($page > $pages) {
		$page = $pages;
	};
	$start = ($page-1) * $nums;
	if ($start < 0) $start = 0;
	$paginator;
};
if($_GET['from'] == 'auto')
	$sqlAuto = "SELECT * FROM `cargo` " . ($_GET['kuzov'] != 0 ? ("WHERE `kuzov` = '" . $site['options']['kuzov'][$_GET['kuzov'] * 1 - 1]. "'" . ($_GET['gruzopod'] != 0 ? " and `gruzopod` = '" . $site['options']['gruzopod'][$_GET['gruzopod'] * 1 - 1] . "'" : "")) : ($_GET['gruzopod'] != 0 ? "WHERE `gruzopod` = '" . $site['options']['gruzopod'][$_GET['gruzopod'] * 1 - 1] . "'" : "WHERE 1")) . ($_GET['manipulator'] ? " and `manipulator` = '1'" : "") . ($_GET['gydrolift'] ? " and `gydrolift` = '1'" : "") . ($_GET['koniki'] ? " and `koniki` = '1'" : "") . ($_GET['help'] ? " and `help` = '1'" : "") . " ORDER BY id DESC LIMIT {$start}, {$nums}";

if($_GET['from'] == 'all') {
	if(strlen(trim($_GET['query'])) < 1)
		$_GET['page'] = 'index';
	else
		$sql = array(
			mysql_query("SELECT * FROM `pages` WHERE LCASE(`name`) LIKE '%" . str_replace(" ", "%' OR LCASE(`name`) LIKE '%", $_GET['query']) . "%' or LCASE(`text`) LIKE '%" . str_replace(" ", "%' OR LCASE(`text`) LIKE '%", $_GET['query']) . "%' ORDER BY id DESC"),
			mysql_query("SELECT * FROM `board` WHERE LCASE(`title`) LIKE '%" . str_replace(" ", "%' OR LCASE(`title`) LIKE '%", $_GET['query']) . "%' or LCASE(`description`) LIKE '%" . str_replace(" ", "%' OR LCASE(`description`) LIKE '%", $_GET['query']) . "%' ORDER BY id DESC"),
			mysql_query("SELECT * FROM `requests` WHERE LCASE(`message`) LIKE '%" . str_replace(" ", "%' OR LCASE(`message`) LIKE '%", $_GET['query']) . "%' ORDER BY id DESC")
		);
} elseif($_GET['from'] == 'auto') $sql = mysql_query($sqlAuto);
else $sql = mysql_query($_GET['kuzov'] != 0 ? ("SELECT * FROM `requests` WHERE `kuzov` = '" . $site['options']['kuzov'][$_GET['kuzov'] * 1 - 1] . "'" . ($_GET['closed'] ? " and `closed` = '0'" : "") . " ORDER BY id DESC") : ("SELECT * FROM `requests` " . ($_GET['closed'] ? "WHERE `closed` = '0'" : "") . " ORDER BY id DESC LIMIT {$start}, {$nums}"));

$entries = array();

if($_GET['from'] == 'all')
	$urlopen = 'all/' . $_GET['query'];
elseif($_GET['from'] == 'auto')
	$urlopen = 'auto/' . $_GET['kuzov'] . '-' . $_GET['gruzopod'] . '-' . $_GET['manipulator'] . '-' . $_GET['gydrolift'] . '-' . $_GET['koniki'] . '-' . $_GET['help'];
else $urlopen = 'tenders/' . $_GET['kuzov'] . '-' . $_GET['closed'];
include ("html2text.inc");

if($_GET['from'] == 'all' && $_GET['page'] == 'search') {
	$row = array();
	while($i_row = mysql_fetch_assoc($sql[0])) {
		$i_row['module'] = 'pages';
		$i_row['name'] = $i_row['name'];
		$i_row['strpos'] = stripos($i_row['text'], $_GET['query']);
		$htmlToText = new Html2Text ($i_row['text'], 15);
		$i_row['text'] = $htmlToText->convert();
		$i_row['text'] = ($i_row['strpos'] > 50 ? '...' : '') . substr($i_row['text'], ($i_row['strpos'] > 50 ? $i_row['strpos'] - 50 : 0), strlen($_GET['query']) + 200) . ($i_row['strpos'] + strlen($_GET['query']) != strlen($i_row['text']) ? '...' : '');
		$i_row['text'] = preg_replace('/(' . $_GET['query'] . ')/i', '<b>$1</b>', $i_row['text']);
		$i_row['url'] = '/' . $i_row['code'];
		$entries[] = $i_row;
	};
	while($i_row = mysql_fetch_assoc($sql[1])) {
		$i_row['module'] = 'board';
		$i_row['name'] = preg_replace('/(' . $_GET['_query'] . ')/i', '<b>$1</b>', $i_row['title']);
		$i_row['text'] = $i_row['description'];
		$i_row['strpos'] = stripos($i_row['text'], $_GET['query']);
		$i_row['text'] = ($i_row['strpos'] > 50 ? '...' : '') . substr($i_row['text'], ($i_row['strpos'] > 50 ? $i_row['strpos'] - 50 : 0), strlen($_GET['query']) + 200) . ($i_row['strpos'] + strlen($_GET['query']) != strlen($i_row['text']) ? '...' : '');
		$i_row['text'] = preg_replace('/(' . $_GET['_query'] . ')/i', '<b>$1</b>', $i_row['text']);
		$i_row['url'] = '/board/' . $i_row['id'];
		$entries[] = $i_row;
	};
	while($i_row = mysql_fetch_assoc($sql[2])) {
		$i_row['module'] = 'tenders';
		$i_row['name'] = '������ �' . $i_row['id'];
		$i_row['text'] = $i_row['message'];
		$i_row['strpos'] = stripos($i_row['text'], $_GET['query']);
		$i_row['text'] = ($i_row['strpos'] > 50 ? '...' : '') . substr($i_row['text'], ($i_row['strpos'] > 50 ? $i_row['strpos'] - 50 : 0), strlen($_GET['query']) + 200) . ($i_row['strpos'] + strlen($_GET['query']) != strlen($i_row['text']) ? '...' : '');
		$i_row['text'] = preg_replace('/(' . $_GET['query'] . ')/i', '<b>$1</b>', $i_row['text']);
		$i_row['url'] = '/tenders/' . $i_row['id'];
		$entries[] = $i_row;
	};
} elseif($_GET['page'] == 'search') {
	if($_GET['page'] == 'search'){
		while($row = mysql_fetch_assoc($sql)) {
			if($_GET['from'] == 'tenders') {
				$row['url'] = array(
					'edit' => '/tenders/edit/' . $row['id'],
					'view' => '/tenders/' . $row['id'],
					'delete' => 'javascript:requests.del(' . $row['id'] . ');',
					'close' => 'javascript:requests.oc(' . $row['id'] . ', 1)',
					'open' => 'javascript:requests.oc(' . $row['id'] . ', 0)'
				);
				$usql = mysql_query("SELECT * FROM `users` WHERE id = " . $row['uid'] . " LIMIT 0, 1");
				$urow = mysql_fetch_assoc($usql);
				$row['author'] = array(
					'id' => $urow['id'],
					'familiya' => $urow['familiya'],
					'imya' => $urow['imya'],
					'otchestvo' => $urow['otchestvo'],
					'birthday' => $urow['birthday'],
					'signature' => $urow['signature'],
					'phone' => ((!$_SESSION['logged'] && $urow['phone_only_reg']) ? false : $urow['phone']),
					'rate' => $urow['rate'],
					'rateClass' => $urow['rate'] > 0 ? 'ratingUp' : ($urow['rate'] < 0 ? 'ratingDown' : ''),
					'active' => $urow['active'],
					'url' => '/user/' . $urow['ulogin'],
					'status' => (strtolower($urow['ulogin']) == $_SESSION['ulogin'] ? 'online' : (time() - $urow['last_entry'] * 1 <= 600 ? 'online': 'offline'))
				);
				$row['message'] = preg_replace('/\n/', '<br>', $row['message']);
				if($row['performer']) {
					$psql = mysql_query("SELECT * FROM `users` WHERE id = " . $row['performer'] . " LIMIT 0, 1");
					$prow = mysql_fetch_assoc($psql);
					$row['performerInfo'] = array(
						'familiya' => $prow['familiya'],
						'imya' => $prow['imya'],
						'otchestvo' => $prow['otchestvo'],
						'birthday' => $prow['birthday'],
						'signature' => $prow['signature'],
						'phone' => ((!$_SESSION['logged'] && $prow['phone_only_reg']) ? false : $prow['phone']),
						'rate' => $prow['rate'],
						'rateClass' => $prow['rate'] > 0 ? 'ratingUp' : ($prow['rate'] < 0 ? 'ratingDown' : ''),
						'active' => $prow['active'],
						'ulogin' => $prow['ulogin'],
						'url' => '/user/' . $prow['ulogin'],
						'status' => (time() - $prow['last_entry'] * 1 <= 600 ? 'online': 'offline'),
						'id' => $prow['id']
					);
				};
				$row['date'] = ruDate($row['time']) . ' ���� � ' . date('G:i', $row['time']);
				$map_geo_from = file_get_contents('http://geocode-maps.yandex.ru/1.x/?format=json&ll=59.147028,57.765608&spn=5.581054,3.082691&rspn=1&geocode=' . urlencode($row['from_address']) . '&key=' . $site['options']['ymaps_key']);
				$map_geo_to = file_get_contents('http://geocode-maps.yandex.ru/1.x/?format=json&ll=59.147028,57.765608&spn=5.581054,3.082691&rspn=1&geocode=' . urlencode($row['to_address']) . '&key=' . $site['options']['ymaps_key']);
				$map_geo_from = json_decode($map_geo_from, true);
				$map_geo_to = json_decode($map_geo_to, true);
				$geo_from = explode(' ', $map_geo_from['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos']);
				$geo_to = explode(' ', $map_geo_to['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos']);
				$geo_ad_from = iconv('utf-8','windows-1251',$map_geo_from['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['text']);
				$geo_ad_to = iconv('utf-8','windows-1251',$map_geo_to['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['text']);
				if($geo_from[0] != '60.597340' && $geo_from[1] != '56.837982' && $geo_to[0] != '60.597340' && $geo_to[1] != '56.837982' && ($geo_from[0] > 59.147028 && $geo_from[0] < 64.728082) && ($geo_from[1] < 57.765608 && $geo_from[1] > 54.682917) && ($geo_from[0] > 0 && $geo_to[0] > 0 && $geo_from[1] > 0 && $geo_to[1] > 0)) {
					$row['map'] = '<div style="display:none;" class="tenderMap" id="tendermap'.$row['id'].'"><div style="width:600px;height:400px;" id="YMaps'.$row['id'].'"></div></div>';
					$row['isMap'] = true;
					$row['toggleMap'] = 'toggle(\'tendermap'.$row['id'].'\', this);req.toggleMap('.$row['id'].', ['.$geo_from[0].', '.$geo_from[1].'], ['.$geo_to[0].', '.$geo_to[1].'], \''.$geo_ad_from.'\', \''.$geo_ad_to.'\');';
				} else $row['isMap'] = false;
			} else {
				$row['url'] = array(
					'edit' => '/auto/edit/' . $row['id'],
					'view' => '/auto/' . $row['id'],
					'delete' => 'javascript:cargo.del(' . $row['id'] . ');',
				);
				$usql = mysql_query("SELECT * FROM `users` WHERE id = " . $row['uid'] . " LIMIT 0, 1");
				$urow = mysql_fetch_assoc($usql);
				$row['author'] = array(
					'familiya' => $urow['familiya'],
					'id' => $urow['id'],
					'imya' => $urow['imya'],
					'otchestvo' => $urow['otchestvo'],
					'birthday' => $urow['birthday'],
					'signature' => $urow['signature'],
					'phone' => ((!$_SESSION['logged'] && $urow['phone_only_reg']) ? false : $urow['phone']),
					'rate' => $urow['rate'],
					'rateClass' => $urow['rate'] > 0 ? 'ratingUp' : ($urow['rate'] < 0 ? 'ratingDown' : ''),
					'active' => $urow['active'],
					'url' => '/user/' . $urow['ulogin'],
					'status' => (strtolower($urow['ulogin']) == $_SESSION['ulogin'] ? 'online' : (time() - $urow['last_entry'] * 1 <= 600 ? 'online': 'offline'))
				);
				$row['rateClass'] = $row['rate'] > 0 ? 'ratingUp' : ($row['rate'] < 0 ? 'ratingDown' : '');
				$row['date'] = ruDate($row['time']) . ' ���� � ' . date('G:i', $row['time']);
				$map_geo_from = file_get_contents('http://geocode-maps.yandex.ru/1.x/?format=json&ll=59.147028,57.765608&spn=5.581054,3.082691&rspn=1&geocode=' . urlencode($row['rajon']) . '&key=' . $site['options']['ymaps_key']);
				$map_geo_from = json_decode($map_geo_from, true);
				$geo_from = explode(' ', $map_geo_from['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos']);
				$geo_ad_from = iconv('utf-8','windows-1251',$map_geo_from['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['text']);
				if($geo_from[0] != '60.597340' && $geo_from[1] != '56.837982' && ($geo_from[0] > 59.147028 && $geo_from[0] < 64.728082) && ($geo_from[1] < 57.765608 && $geo_from[1] > 54.682917)) {
					$row['map'] = '<div style="display:none;" class="tenderMap" id="truckmap'.$row['id'].'"><div style="width:600px;height:400px;" id="YMapsCargo'.$row['id'].'"></div></div>';
					$row['isMap'] = true;
					$row['toggleMap'] = 'toggle(\'truckmap'.$row['id'].'\', this);cargo.toggleMap('.$row['id'].', ['.$geo_from[0].', '.$geo_from[1].'], \''.$geo_ad_from.'\');';
				} else $row['isMap'] = false;
					if($row['photo1']) {
						$row['photo'] = array(
							 '/upload/photo-' . $row['id'] . '-1.jpg',
							 '/upload/r/photo-' . $row['id'] . '-1.jpg'
						);
					};
				$row['cargo']['entries'][] = $row;
			};
			$entries[] = $row;
		};
	};
};

$locationSearch = '/search/' . $_GET['from'] . '/' . ($_GET['from'] == 'auto' ? ($_GET['kuzov'] . '-' . $_GET['gruzopod'] . '-' . $_GET['manipulator'] . '-' . $_GET['gydrolift'] . '-' . $_GET['koniki'] . '-' . $_GET['help']) : ($_GET['from'] == 'tenders' ? $_GET['kuzov'] . '-' . $_GET['closed'] : ''));

$neighbours = 3;
$left_neighbour = $page - $neighbours;
if ($left_neighbour < 1) $left_neighbour = 1;
$right_neighbour = $page + $neighbours;
if ($right_neighbour > $pages) $right_neighbour = $pages;
if ($page > 1) {
	$paginator .= '<a href="' . $locationSearch . '/' . ($page-1) . '">&larr; �����</a>';
};
for ($i=$left_neighbour; $i<=$right_neighbour; $i++) {
	if ($i != $page) {
		$paginator .= '<a href="' . $locationSearch . '/' . $i . '">' . $i . '</a>';
	} else {
		$paginator .= '<strong>' . $i . '</strong>';
	};
};
if ($page < $pages) {
	$paginator .= '<a href="' . $locationSearch . '/' . ($page+1) . '">������ &rarr;</a>';
}

$smarty -> assign('search', array(
	'col' => $elements ? $elements : count($entries),
	'pages' => $pages,
	'paginator' => $paginator,
	'colText' => declension(count($entries), array('��', '��', '��')),
	'entries' => $entries,
	'params' => $_GET
));

$smarty -> assign('title', '����� ' . ($_GET['_query'] ? '�� ������� �' . $_GET['_query'] . '�' : '') . ' - ' . $site['name']);

$smarty -> display('static.tpl');
?>