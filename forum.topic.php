<?php
$script_content = 1;
$script = 'forum.topic';
$script_uri = 'forum';

include('params.php');

$sql = mysql_query("SELECT * FROM `forum_forums` WHERE `id` = " . $_GET['forum']);
$forum = mysql_fetch_assoc($sql);
notLoggedError(!mysql_num_rows($sql));
$sql = mysql_query("SELECT * FROM `forum_topics` WHERE `id` = " . $_GET['id']);
notLoggedError(!mysql_num_rows($sql));
if(!$_SESSION['forum_viewed_topic_' . $_GET['id']]) {
	mysql_query("UPDATE `forum_topics` SET `views` = `views` + 1 WHERE `id` = " . $_GET['id']);
	$_SESSION['forum_viewed_topic_' . $_GET['id']] = true;
};

$topic = mysql_fetch_assoc($sql);

$nums = 15;
if(isset($_GET['page'])) {
	$page = intval($_GET['page']);
} else {
	$page = 1;
};

$sql = mysql_query("SELECT COUNT(*) AS `counter` FROM `forum_posts` WHERE `topic` = " . $_GET['id'] . " ORDER BY `id`");
$row = mysql_fetch_assoc($sql);
$elements = $row['counter'];

$pages = ceil($elements/$nums);
if($page < 1) {
	$page = 1;
} elseif($page > $pages) {
	$page = $pages;
};
$start = ($page-1) * $nums;
if ($start < 0) $start = 0;
$sql = mysql_query("SELECT * FROM `forum_posts` WHERE `topic` = " . $_GET['id'] . " ORDER BY `id` LIMIT {$start}, {$nums}");
$paginator;
function get_video_com($link){
	if(strpos($link, '&')) $link = substr($link, strpos($link, 'v=') + 2, strpos(substr($link, strpos($link, 'v=') + 2), '&'));
	else $link = substr($link, strpos($link, 'v=') + 2);
	$link = '<div style="margin:5px 0;"><object width="480" height="385"><param name="movie" value="http://www.youtube.com/v/' . $link . '"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/' .$link . '" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="480" height="385"></embed></object></div>';
	return $link;
};
function get_video_be($link){
	$link = '<div style="margin:5px 0;"><object width="480" height="385"><param name="movie" value="http://www.youtube.com/v/' . $link . '"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/' .$link . '" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="480" height="385"></embed></object></div>';
	return $link;
};
while($row = mysql_fetch_assoc($sql)){
	$usql = mysql_query("SELECT * FROM `users` WHERE id = " . $row['uid'] . " LIMIT 0, 1");
	$urow = mysql_fetch_assoc($usql);
	$row['author'] = array(
		'familiya' => $urow['familiya'],
		'id' => $urow['id'],
		'imya' => $urow['imya'],
		'otchestvo' => $urow['otchestvo'],
		'userpic' => $urow['userpic'],
		'birthday' => $urow['birthday'],
		'signature' => $urow['signature'],
		'phone' => ((!$_SESSION['logged'] && $urow['phone_only_reg']) ? false : $urow['phone']),
		'rate' => $urow['rate'],
		'rateClass' => $urow['rate'] > 0 ? 'ratingUp' : ($urow['rate'] < 0 ? 'ratingDown' : ''),
		'active' => $urow['active'],
		'url' => '/user/' . $urow['ulogin'],
		'status' => (strtolower($urow['ulogin']) == $_SESSION['ulogin'] ? 'online' : (time() - $urow['last_entry'] * 1 <= 600 ? 'online': 'offline'))
	);
	$row['text'] = preg_replace('/\n/', '<br>', $row['text']);
	$row['text'] = preg_replace('#\[b\](.*?)\[/b\]#si', '<b>$1</b>', $row['text']);
	$row['text'] = preg_replace('#\[i\](.*?)\[/i\]#si', '<i>$1</i>', $row['text']);
	$row['text'] = preg_replace('/\[img\](http:\/\/([a-zA-Z0-9\-\.]+)\.([a-zA-Z]{2,7})\/([a-zA-Z0-9_\&\?\/\-=+\(\)\[\]\$\.]+)\.(png|gif|jpg|jpeg|bmp|tiff))\[\/img\]/', '<img src="$1"/>', $row['text']);
	$row['text'] = preg_replace('/\[url\](http:\/\/([a-zA-Z0-9\-\.]+)\.([a-zA-Z]{2,7})([a-zA-Z0-9_\&\?\/\-=+\(\)\[\]\$\.]*))\[\/url\]/', '<a rel="nofollow" target="_blank" href="/go.php?url=$1">$1</a>', $row['text']);
	$row['text'] = preg_replace('/\[url=(http:\/\/([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,7})([a-zA-Z0-9\&\?\/\-=+\(\)\[\]\$\.]*))\]([a-zA-Z0-9�-��-�:_\.\/\-=\s\r]+)\[\/url\]/', '<a rel="nofollow" target="_blank" href="/go.php?url=$1">$5</a>', $row['text']);
	$row['text'] = preg_replace("/\[video\](http:\/\/([w]{3}\.)?youtube\.com\/(.*))\[\/video\]/ies", "get_video_com('\\1')", $row['text']);
	$row['text'] = preg_replace("/\[video\](http:\/\/([w]{3}\.)?youtu\.be\/(.*))\[\/video\]/ies", "get_video_be('\\3')", $row['text']);
	
	$row['text'] = preg_replace('/\[quote=([a-zA-Z0-9\-_]+)\](.*)\[\/quote]/i', '<div class="forumQuote">$2</div>', $row['text']);
	$row['url'] = '/forum/' . $_GET['id'] . '/' . $row['id'];
	$row['date'] = date('d.m.Y') === date('d.m.Y', $row['time']) ? '�������' : ((date('d') * 1 - 1) . date('.m.Y') === date('d.m.Y', $row['time']) ? '�����' : date('d.m.Y', $row['time']));
	$row['unix'] = $row['time'];
	$row['time'] = date('H:i', $row['time']);
	$posts[] = $row;
};
$neighbours = 3;
$left_neighbour = $page - $neighbours;
if ($left_neighbour < 1) $left_neighbour = 1;
$right_neighbour = $page + $neighbours;
if ($right_neighbour > $pages) $right_neighbour = $pages;
if ($page > 1) {
	$paginator .= '<span class="pagePrew"><a href="/forum/' . $_GET['forum'] . '/' . $_GET['id'] . '/' . ($page-1) . '">&larr; �����</a></span>';
};
for ($i=$left_neighbour; $i<=$right_neighbour; $i++) {
	if ($i != $page) {
		$paginator .= '<a class="navCell" href="/forum/' . $_GET['forum'] . '/' . $_GET['id'] . '/' . $i . '">' . $i . '</a>';
	} else {
		$paginator .= '<span class="navCell">' . $i . '</span>';
	};
};
if ($page < $pages) {
	$paginator .= '<span class="pageNext"><a href="/forum/' . $_GET['forum'] . '/' . $_GET['id'] . '/' . ($page+1) . '">������ &rarr;</a></span>';
}
$smarty -> assign('forum', $forum);

$smarty -> assign('list', array(
	'page' => $page,
	'pages' => $pages,
	'paginator' => $paginator
));

$smarty -> assign('last_post_num', $posts[count($posts) - 1]['num']);

$smarty -> assign('topic', $topic);

$smarty -> assign('posts', $posts);

$smarty -> assign('title', $topic['name'] . ' - ' . $forum['name'] . ' - ����� - ' . $site['name']);

$smarty -> display('static.tpl');
?>