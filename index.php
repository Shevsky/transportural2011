<?php
$script_content = 1;
$script = 'requests';
$script_uri = 'requests';

include('params.php');

$nums = $site['options']['show_on_page_requests'];
if(isset($_GET['page'])) {
	$page = intval($_GET['page']);
} else {
	$page = 1;
};

if($_SESSION['logged'] == 1) $sql = mysql_query("SELECT COUNT(*) AS `counter` FROM `requests` WHERE (`closed` = 0 || `performer` = '" . $_SESSION['id'] . "' || `uid` = '" . $_SESSION['id'] . "')");
else $sql = mysql_query("SELECT COUNT(*) AS `counter` FROM `requests` WHERE `closed` = 0");
$row = mysql_fetch_assoc($sql);
$elements = $row['counter'];

$pages = ceil($elements/$nums);
if($page < 1) {
	$page = 1;
} elseif($page > $pages) {
	$page = $pages;
};
$start = ($page-1) * $nums;
if ($start < 0) $start = 0;

if($_SESSION['logged'] == 1) $sql = mysql_query("SELECT * FROM `requests` WHERE (`closed` = 0 || `performer` = '" . $_SESSION['id'] . "' || `uid` = '" . $_SESSION['id'] . "') ORDER BY id DESC LIMIT {$start}, {$nums}");
else $sql = mysql_query("SELECT * FROM `requests` WHERE `closed` = 0 ORDER BY id DESC LIMIT {$start}, {$nums}");
$entries = array();
$paginator;
while($row = mysql_fetch_assoc($sql)) {
	$usql = mysql_query("SELECT * FROM `users` WHERE id = " . $row['uid'] . " LIMIT 0, 1");
	$urow = mysql_fetch_assoc($usql);
	$row['url'] = array(
		'edit' => '/tenders/edit/' . $row['id'],
		'view' => '/tenders/' . $row['id'],
		'delete' => 'javascript:requests.del(' . $row['id'] . ');',
		'close' => 'javascript:requests.oc(' . $row['id'] . ', 1)',
		'open' => 'javascript:requests.oc(' . $row['id'] . ', 0)'
	);
	$row['message'] = preg_replace('/\n/', '<br>', $row['message']);
	$row['author'] = array(
		'id' => $urow['id'],
		'familiya' => $urow['familiya'],
		'imya' => $urow['imya'],
		'otchestvo' => $urow['otchestvo'],
		'birthday' => $urow['birthday'],
		'signature' => $urow['signature'],
		'phone' => ((!$_SESSION['logged'] && $urow['phone_only_reg']) ? false : $urow['phone']),
		'rate' => $urow['rate'],
		'rateClass' => $urow['rate'] > 0 ? 'ratingUp' : ($urow['rate'] < 0 ? 'ratingDown' : ''),
		'active' => $urow['active'],
		'url' => '/user/' . $urow['ulogin'],
		'status' => (strtolower($urow['ulogin']) == $_SESSION['ulogin'] ? 'online' : (time() - $urow['last_entry'] * 1 <= 600 ? 'online': 'offline'))
	);
	if($row['performer']) {
		$psql = mysql_query("SELECT * FROM `users` WHERE id = " . $row['performer'] . " LIMIT 0, 1");
		$prow = mysql_fetch_assoc($psql);
		$row['performerInfo'] = array(
			'familiya' => $prow['familiya'],
			'imya' => $prow['imya'],
			'otchestvo' => $prow['otchestvo'],
			'birthday' => $prow['birthday'],
			'signature' => $prow['signature'],
			'phone' => ((!$_SESSION['logged'] && $prow['phone_only_reg']) ? false : $prow['phone']),
			'rate' => $prow['rate'],
			'rateClass' => $prow['rate'] > 0 ? 'ratingUp' : ($prow['rate'] < 0 ? 'ratingDown' : ''),
			'active' => $prow['active'],
			'ulogin' => $prow['ulogin'],
			'url' => '/user/' . $prow['ulogin'],
			'rating' => 0,
			'status' => (time() - $prow['last_entry'] * 1 <= 600 ? 'online': 'offline'),
			'id' => $prow['id']
		);
	};
	$row['date'] = ruDate($row['time']) . ' � ' . date('G:i', $row['time']);
	$map_geo_from = file_get_contents('http://geocode-maps.yandex.ru/1.x/?format=json&ll=59.147028,57.765608&spn=5.581054,3.082691&rspn=1&geocode=' . urlencode($row['from_address']) . '&key=' . $site['options']['ymaps_key']);
	$map_geo_to = file_get_contents('http://geocode-maps.yandex.ru/1.x/?format=json&ll=59.147028,57.765608&spn=5.581054,3.082691&rspn=1&geocode=' . urlencode($row['to_address']) . '&key=' . $site['options']['ymaps_key']);
	$map_geo_from = json_decode($map_geo_from, true);
	$map_geo_to = json_decode($map_geo_to, true);
	$geo_from = explode(' ', $map_geo_from['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos']);
	$geo_to = explode(' ', $map_geo_to['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos']);
	$geo_ad_from = iconv('utf-8','windows-1251',$map_geo_from['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['text']);
	$geo_ad_to = iconv('utf-8','windows-1251',$map_geo_to['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['text']);
	if($geo_from[0] != '60.597340' && $geo_from[1] != '56.837982' && $geo_to[0] != '60.597340' && $geo_to[1] != '56.837982' && ($geo_from[0] > 59.147028 && $geo_from[0] < 64.728082) && ($geo_from[1] < 57.765608 && $geo_from[1] > 54.682917) && ($geo_from[0] > 0 && $geo_to[0] > 0 && $geo_from[1] > 0 && $geo_to[1] > 0)) {
		$row['map'] = '<div style="display:none;" class="tenderMap" id="tendermap'.$row['id'].'"><div style="width:600px;height:400px;" id="YMaps'.$row['id'].'"></div></div>';
		$row['isMap'] = true;
		$row['toggleMap'] = 'toggle(\'tendermap'.$row['id'].'\', this);req.toggleMap('.$row['id'].', ['.$geo_from[0].', '.$geo_from[1].'], ['.$geo_to[0].', '.$geo_to[1].'], \''.$geo_ad_from.'\', \''.$geo_ad_to.'\');';
	} else $row['isMap'] = false;
	$entries[] = $row;
};
$neighbours = 3;
$left_neighbour = $page - $neighbours;
if ($left_neighbour < 1) $left_neighbour = 1;
$right_neighbour = $page + $neighbours;
if ($right_neighbour > $pages) $right_neighbour = $pages;
if ($page > 1) {
	$paginator .= '<a href="/tenders/page/' . ($page-1) . '">&larr; �����</a>';
};
for ($i=$left_neighbour; $i<=$right_neighbour; $i++) {
	if ($i != $page) {
		$paginator .= '<a href="/tenders/page/' . $i . '">' . $i . '</a>';
	} else {
		$paginator .= '<strong>' . $i . '</strong>';
	};
};
if ($page < $pages) {
	$paginator .= '<a href="/tenders/page/' . ($page+1) . '">������ &rarr;</a>';
}

$smarty -> assign('list', array(
	'pages' => $pages,
	'entries' => $entries,
	'paginator' => $paginator,
	'action' => $_GET['act'],
	'url' => array(
		'add' => array(
			'requests' => '/tenders/add'
		)
	)
));

$smarty -> assign('title', $site['name']);

$smarty -> display('static.tpl');
?>