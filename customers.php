<?php
$script_content = 1;
$script = 'customers';
$script_uri = 'customers';

include('params.php');

$nums = $site['options']['nums'];
if(isset($_GET['page'])) {
	$page = intval($_GET['page']);
} else {
	$page = 1;
};

$sql = mysql_query("SELECT COUNT(*) AS `counter` FROM `users` ORDER BY `customer_reputation` DESC");
$row = mysql_fetch_assoc($sql);
$elements = $row['counter'];

$pages = ceil($elements/$nums);
if($page < 1) {
	$page = 1;
} elseif($page > $pages) {
	$page = $pages;
};
$start = ($page-1) * $nums;
if ($start < 0) $start = 0;

$sql = mysql_query("SELECT * FROM `users` ORDER BY `customer_reputation` DESC LIMIT {$start}, {$nums}");
$entries = array();
$paginator;
while($row = mysql_fetch_assoc($sql)) {
	$row['signature'] = preg_replace('/\n/', '<br>', $row['signature']);
	$row['status'] = (strtolower($row['ulogin']) == $_SESSION['ulogin'] ? 'online' : (time() - $row['last_entry'] * 1 <= 600 ? 'online': 'offline'));
	$row['url'] = '/user/' . $row['ulogin'];
	$users[] = $row;
};
$neighbours = 3;
$left_neighbour = $page - $neighbours;
if ($left_neighbour < 1) $left_neighbour = 1;
$right_neighbour = $page + $neighbours;
if ($right_neighbour > $pages) $right_neighbour = $pages;
if ($page > 1) {
	$paginator .= '<a href="/customers/' . ($page-1) . '">&larr; �����</a>';
};
for ($i=$left_neighbour; $i<=$right_neighbour; $i++) {
	if ($i != $page) {
		$paginator .= '<a href="/customers/' . $i . '">' . $i . '</a>';
	} else {
		$paginator .= '<strong>' . $i . '</strong>';
	};
};
if ($page < $pages) {
	$paginator .= '<a href="/customers/' . ($page+1) . '">������ &rarr;</a>';
}

$smarty -> assign('list', array(
	'pages' => $pages,
	'users' => $users,
	'paginator' => $paginator
));

$smarty -> append('site', array(
	'openPage' => 'customers'
), true);

$smarty -> assign('title', '���������������� - ' . $site['name']);

$smarty -> display('static.tpl');
?>