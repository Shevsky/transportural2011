<?php
$script_content = 1;
session_start();
$scriptisown = ((strtolower($_GET['login']) == $_SESSION['ulogin'] || $_SESSION['group'] == 1) ? 'profile.edit' : 'profile');
$script = $_GET['act'] == 'view' ? 'profile' : $scriptisown;

include('params.php');

if($_GET['act'] == 'post' && ($_GET['login'] == $_SESSION['ulogin'] || $_SESSION['group'] == 1)) {
		$value = $_FILES['userpic'];
		require('libs/uploader.class.php');
		if($value['size'] > 0) {
			if($_POST['userpicLast']) unlink($site['root'] . $_POST['userpicLast']);
			$userpicName = strtolower($_GET['login']) . '-' . substr(md5('photo-' . strtolower($_GET['login']) . '-' . rand()), 0, 10);
			$userpicQuery = ", `userpic` = '/userpics/" . $userpicName . ".jpg'";
			if ($value['type'] == 'image/jpeg')
				$source = imagecreatefromjpeg($value['tmp_name']);
			elseif ($value['type'] == 'image/png')
				$source = imagecreatefrompng($value['tmp_name']);
			elseif ($value['type'] == 'image/gif')
				$source = imagecreatefromgif($value['tmp_name']);
			else
				$source = imagecreatefromjpeg($value['tmp_name']);
				$width = imagesx($source);
				$height = imagesy($source);
				$uploader = new uploader();
				$uploader->source = $value;
				$uploader->autoName = $userpicName;
				if($width > 100 || $height > 100) {
					$uploader->destDir = $site['root'] . '/temp/';
					$uploader->resizeDir = $site['root'] . '/userpics/';
					$uploader->resize($uploader->upload(''), 100, 100);
					unlink($site['root'] . '/temp/' . $userpicName . '.jpg');
				} else {
					$uploader->destDir = $site['root'] . '/userpics/';
					$uploader->upload('');
				};
		};
		if(strlen(trim($_POST['password'])) > 6 && $_POST['password'] == $_POST['repassword']) {
			$newPassword = ", `password` = '" . md5($_POST['password'] . '_AMX') . "'";
		};
		if(!$userpicQuery) $userpicQuery = '';
	$sql = mysql_query("UPDATE users SET `imya`='".$_POST['imya']."', `familiya`='".$_POST['familiya']."', `otchestvo`='".$_POST['otchestvo']."', `signature`='".htmlspecialchars($_POST['signature'])."', `phone`='".$_POST['phone']."', `phone_only_reg` = '".$_POST['phone_only_reg']."', `birthday_d`='".$_POST['birthday_d']."', `birthday_y`='".$_POST['birthday_y']."', `birthday_m`='".$_POST['birthday_m']."', `sex`='".$_POST['sex']."'".$userpicQuery.$newPassword.", `type_faces` = '" . $_POST['type_faces'] . "', `company_name` = '" . $_POST['company_name'] . "' WHERE `ulogin`='".strtolower($_GET['login'])."'");
	if($_SESSION['ulogin'] == strtolower($_GET['login']))
		foreach($_POST as $key => $value){
			$_SESSION[$key] = $value;
		};
	header("Location: /user/".$_GET['login']);
	exit;
} else {
$sql = mysql_query("SELECT * FROM `users` WHERE ulogin = '" . strtolower($_GET['login']) . "'");
};
notLoggedError(mysql_num_rows($sql) == 0);
if($_GET['act'] == 'edit') $noLoad = true;

$row = mysql_fetch_assoc($sql);
$requests_sql = mysql_query("SELECT * FROM `requests` WHERE uid = ".$row['id']." ORDER BY id DESC");
$row['requestsCount'] = mysql_num_rows($requests_sql);
$cargo_sql = mysql_query("SELECT * FROM `cargo` WHERE uid = ".$row['id']." ORDER BY id DESC");
$row['cargoCount'] = mysql_num_rows($cargo_sql);
$board_sql = mysql_query("SELECT * FROM `board` WHERE uid = ".$row['id']." ORDER BY id DESC");
$row['boardCount'] = mysql_num_rows($board_sql);
$reviews_sql = mysql_query("SELECT * FROM `otzyvy` WHERE toUid = ".$row['id']." ORDER BY id DESC");
$row['reviewsCount'] = mysql_num_rows($reviews_sql);
if(mysql_num_rows($sql) > 0){
$row['url'] = array(
	'edit' => '/user/' . strtolower($_GET['login']) . '/edit',
	'page' => '/user/' . strtolower($_GET['login']),
	'post' => '/user/' . strtolower($_GET['login']) . '/edit/post',
	'information' => '/user/' . strtolower($_GET['login']) . '/information',
	'auto' => '/user/' . strtolower($_GET['login']) . '/auto',
	'tenders' => '/user/' . strtolower($_GET['login']) . '/tenders',
	'board' => '/user/' . strtolower($_GET['login']) . '/board',
	'reviews' => '/user/' . strtolower($_GET['login']) . '/reviews'
);
if($_GET['show']) $needView = $_GET['show'];
else {
	if($row['signature']) $needView = 'information';
	elseif($row['cargoCount'] > 0) $needView = 'auto';
	elseif($row['requestsCount'] > 0) $needView = 'tenders';
	elseif($row['boardCount'] > 0) $needView = 'board';
	elseif($row['reviewsCount'] > 0) $needView = 'reviews';
	else $needView = false;
};
$row['active'] = $needView;
if(!$noLoad){
	if($needView == 'tenders') {
		while($requests_row = mysql_fetch_assoc($requests_sql)) {
			$requests_row['url'] = array(
				'edit' => '/tenders/edit/' . $requests_row['id'],
				'view' => '/tenders/' . $requests_row['id'],
				'delete' => 'javascript:requests.del(' . $requests_row['id'] . ');',
				'close' => 'javascript:requests.oc(' . $requests_row['id'] . ', 1)',
				'open' => 'javascript:requests.oc(' . $requests_row['id'] . ', 0)'
			);
			$requests_row['author'] = array(
				'id' => $_SESSION['id']
			);
			$requests_row['message'] = preg_replace('/\n/', '<br>', $requests_row['message']);
			if($requests_row['performer']) {
				$psql = mysql_query("SELECT * FROM `users` WHERE id = " . $requests_row['performer'] . " LIMIT 0, 1");
				$prow = mysql_fetch_assoc($psql);
				$requests_row['performerInfo'] = array(
					'familiya' => $prow['familiya'],
					'imya' => $prow['imya'],
					'otchestvo' => $prow['otchestvo'],
					'birthday' => $prow['birthday'],
					'signature' => $prow['signature'],
					'phone' => ((!$_SESSION['logged'] && $prow['phone_only_reg']) ? false : $prow['phone']),
					'rate' => $prow['rate'],
					'rateClass' => $prow['rate'] > 0 ? 'ratingUp' : ($prow['rate'] < 0 ? 'ratingDown' : ''),
					'active' => $prow['active'],
					'ulogin' => $prow['ulogin'],
					'url' => '/user/' . $prow['ulogin'],
					'status' => (time() - $prow['last_entry'] * 1 <= 600 ? 'online': 'offline'),
					'id' => $prow['id']
				);
			};
			$requests_row['date'] = ruDate($requests_row['time']) . ' ���� � ' . date('G:i', $requests_row['time']);
			$map_geo_from = file_get_contents('http://geocode-maps.yandex.ru/1.x/?format=json&ll=59.147028,57.765608&spn=5.581054,3.082691&rspn=1&geocode=' . urlencode($requests_row['from_address']) . '&key=' . $site['options']['ymaps_key']);
			$map_geo_to = file_get_contents('http://geocode-maps.yandex.ru/1.x/?format=json&ll=59.147028,57.765608&spn=5.581054,3.082691&rspn=1&geocode=' . urlencode($requests_row['to_address']) . '&key=' . $site['options']['ymaps_key']);
			$map_geo_from = json_decode($map_geo_from, true);
			$map_geo_to = json_decode($map_geo_to, true);
			$geo_from = explode(' ', $map_geo_from['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos']);
			$geo_to = explode(' ', $map_geo_to['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos']);
			$geo_ad_from = iconv('utf-8','windows-1251',$map_geo_from['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['text']);
			$geo_ad_to = iconv('utf-8','windows-1251',$map_geo_to['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['text']);
			if($geo_from[0] != '60.597340' && $geo_from[1] != '56.837982' && $geo_to[0] != '60.597340' && $geo_to[1] != '56.837982' && ($geo_from[0] > 59.147028 && $geo_from[0] < 64.728082) && ($geo_from[1] < 57.765608 && $geo_from[1] > 54.682917) && ($geo_from[0] > 0 && $geo_to[0] > 0 && $geo_from[1] > 0 && $geo_to[1] > 0)) {
				$requests_row['map'] = '<div style="display:none;" class="tenderMap" id="tendermap'.$requests_row['id'].'"><div style="width:600px;height:400px;" id="YMaps'.$requests_row['id'].'"></div></div>';
				$requests_row['isMap'] = true;
				$requests_row['toggleMap'] = 'toggle(\'tendermap'.$requests_row['id'].'\', this);req.toggleMap('.$requests_row['id'].', ['.$geo_from[0].', '.$geo_from[1].'], ['.$geo_to[0].', '.$geo_to[1].'], \''.$geo_ad_from.'\', \''.$geo_ad_to.'\');';
			} else $requests_row['isMap'] = false;
			$row['requests']['entries'][] = $requests_row;
		};
	};

	if($needView == 'auto') {
		while($cargo_row = mysql_fetch_assoc($cargo_sql)) {
			$cargo_row['url'] = array(
				'edit' => '/auto/edit/' . $cargo_row['id'],
				'view' => '/auto/' . $cargo_row['id'],
				'delete' => 'javascript:cargo.del(' . $cargo_row['id'] . ');',
			);
			$cargo_row['author'] = array(
				'id' => $_SESSION['id']
			);
			$cargo_row['rateClass'] = $cargo_row['rate'] > 0 ? 'ratingUp' : ($cargo_row['rate'] < 0 ? 'ratingDown' : '');
			$cargo_row['date'] = ruDate($cargo_row['time']) . ' ���� � ' . date('G:i', $cargo_row['time']);
			$map_geo_from = file_get_contents('http://geocode-maps.yandex.ru/1.x/?format=json&ll=59.147028,57.765608&spn=5.581054,3.082691&rspn=1&geocode=' . urlencode($cargo_row['rajon']) . '&key=' . $site['options']['ymaps_key']);
			$map_geo_from = json_decode($map_geo_from, true);
			$geo_from = explode(' ', $map_geo_from['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos']);
			$geo_ad_from = iconv('utf-8','windows-1251',$map_geo_from['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['text']);
			if($geo_from[0] != '60.597340' && $geo_from[1] != '56.837982' && ($geo_from[0] > 59.147028 && $geo_from[0] < 64.728082) && ($geo_from[1] < 57.765608 && $geo_from[1] > 54.682917)) {
				$cargo_row['map'] = '<div style="display:none;" class="tenderMap" id="truckmap'.$cargo_row['id'].'"><div style="width:600px;height:400px;" id="YMapsCargo'.$cargo_row['id'].'"></div></div>';
				$cargo_row['isMap'] = true;
				$cargo_row['toggleMap'] = 'toggle(\'truckmap'.$cargo_row['id'].'\', this);cargo.toggleMap('.$cargo_row['id'].', ['.$geo_from[0].', '.$geo_from[1].'], \''.$geo_ad_from.'\');';
			} else $cargo_row['isMap'] = false;
				if($cargo_row['photo1']) {
					$cargo_row['photo'] = array(
						 '/upload/photo-' . $cargo_row['id'] . '-1.jpg',
						 '/upload/r/photo-' . $cargo_row['id'] . '-1.jpg'
					);
				};
			$row['cargo']['entries'][] = $cargo_row;
		};
	};

	if($needView == 'board') {
		while($board_row = mysql_fetch_assoc($board_sql)) {
			$board_row['url'] = array(
				'edit' => '/board/edit/' . $board_row['id'],
				'view' => '/board/' . $board_row['id'],
				'delete' => 'javascript:board.del(' . $board_row['id'] . ');',
			);
			$board_row['author'] = array(
				'id' => $_SESSION['id']
			);
			$board_row['message'] = preg_replace('/\n/', '<br>', $board_row['message']);
			$board_row['rateClass'] = $board_row['rate'] > 0 ? 'ratingUp' : ($board_row['rate'] < 0 ? 'ratingDown' : '');
			$board_row['date'] = ruDate($board_row['time']) . ' ���� � ' . date('G:i', $board_row['time']);
			for($i = 0; $i <= 10; $i++) {
				if($board_row['photo' . ($i + 1)]) {
					$board_row['photo'][] = array(
							 '/upload/'.md5('photo-' . $board_row['id'] . '-' . ($i + 1) . ($board_row['photo' . ($i + 1)] >= 5 ? '-' . $board_row['photo' . ($i + 1)] : '')).'.jpg',
							 '/upload/r/'.md5('photo-' . $board_row['id'] . '-' . ($i + 1) . ($board_row['photo' . ($i + 1)] >= 5 ? '-' . $board_row['photo' . ($i + 1)] : '')) . '.jpg'
					);
				};
			};
			$row['board']['entries'][] = $board_row;
		};
	};
	
	if($needView == 'reviews') {
		while($reviews_row = mysql_fetch_assoc($reviews_sql)) {
			$reviews_row['url'] = array(
				'profile' => '/user/' . $reviews_row['id'],
				'view' => '/board/' . $board_row['id'],
				'delete' => 'javascript:board.del(' . $board_row['id'] . ');',
			);
			$u_reviewssql = mysql_query("SELECT * FROM `users` WHERE id = " . $reviews_row['uid'] . " LIMIT 0, 1");
			$u_reviewsrow = mysql_fetch_assoc($u_reviewssql);
			$reviews_row['message'] = preg_replace('/\n/', '<br>', $reviews_row['message']);
			$reviews_row['date'] = ruDate($reviews_row['time']) . ' ���� � ' . date('H:i', $reviews_row['time']);
			$reviews_row['author'] = array(
				'id' => $u_reviewsrow['id'],
				'familiya' => $u_reviewsrow['familiya'],
				'imya' => $u_reviewsrow['imya'],
				'otchestvo' => $u_reviewsrow['otchestvo'],
				'birthday' => $u_reviewsrow['birthday'],
				'signature' => $u_reviewsrow['signature'],
				'phone' => ((!$_SESSION['logged'] && $u_reviewsrow['phone_only_reg']) ? false : $u_reviewsrow['phone']),
				'rate' => $u_reviewsrow['rate'],
				'rateClass' => $u_reviewsrow['rate'] > 0 ? 'ratingUp' : ($u_reviewsrow['rate'] < 0 ? 'ratingDown' : ''),
				'active' => $u_reviewsrow['active'],
				'url' => '/user/' . $u_reviewsrow['ulogin'],
				'status' => (strtolower($u_reviewsrow['ulogin']) == $_SESSION['ulogin'] ? 'online' : (time() - $u_reviewsrow['last_entry'] * 1 <= 600 ? 'online': 'offline'))
			);
			$row['reviews'][] = $reviews_row;
		};
	};
};
if($_GET['act'] == 'view') $row['signature'] = preg_replace('/\n/', '<br>', $row['signature']);
$row['status'] = (strtolower($_GET['login']) == $_SESSION['ulogin'] ? 'online' : (time() - $row['last_entry'] * 1 <= 600 ? 'online': 'offline'));
$dateMounth = array(
	0,
	'������',
	'�������',
	'�����',
	'������',
	'���',
	'����',
	'����',
	'�������',
	'��������',
	'�������',
	'������',
	'�������'
);
if($row['ban'] == 1) $row['ban_date'] = ruDate($row['ban_to']);
$row['birthday'] = ($row['birthday_d'] && $row['birthday_m'] ? $row['birthday_d'] . ' ' . $dateMounth[$row['birthday_m']] . ($row['birthday_y'] ? ' ' . $row['birthday_y'] . ' ����' : '') : false);
if(!$_SESSION['logged'] && $row['phone_only_reg']) $row['phone'] = false;
$row['userpicOriginal'] = $row['userpic'];
$row['userpic'] = $site['domain'] ? ($row['userpic'] ? 'http://' . $site['domain'] . $row['userpic'] : false) : $row['userpic'];
$row['sex'] = $row['sex'] == 1 ? '�������' : ($row['sex'] == 2 ? '�������' : false);
$row['rateClass'] = $row['rate'] > 0 ? 'ratingUp' : ($row['rate'] < 0 ? 'ratingDown' : '');
$row['freighter_reputationClass'] = $row['freighter_reputation'] > 0 ? 'ratingUp' : ($row['freighter_reputation'] < 0 ? 'ratingDown' : '');
$row['customer_reputationClass'] = $row['customer_reputation'] > 0 ? 'ratingUp' : ($row['customer_reputation'] < 0 ? 'ratingDown' : '');
$profile = $row;

$smarty -> assign('profile', $profile);
} else header("Location: /404.php");

if(isset($_GET['new'])) $smarty -> assign('message', str_replace('{$profile.url.edit}', $row['url']['edit'], $site['options']['messages']['500']));

$smarty -> assign('title', $profile['imya'] . ($profile['familiya'] ? ' ' . $profile['familiya'] : '') . ' - ' . $site['name']);

$smarty -> display('static.tpl');
?>