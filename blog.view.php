<?php
$script_content = 1;
$script = 'blog.view';
$script_uri = 'blog';

include('params.php');

$sql = mysql_query("SELECT * FROM `blog` WHERE id = '" . $_GET['id'] . "'");

$row = mysql_fetch_assoc($sql);
$row['date'] = ruDate($row['time']);
$row['url'] = array(
	'view' => '/blog/' . $row['id'],
	'edit' => '/blog/edit/' . $row['id'],
	'delete' => 'javascript:blog.del(' . $row['id'] . ');'
);
$row['message'] = str_replace('$CUT$', '', $row['message']);
$row['message'] = preg_replace('/\n/', '<br>', $row['message']);
$row['message'] = preg_replace('#\[b\](.*?)\[/b\]#si', '<b>$1</b>', $row['message']);
$row['message'] = preg_replace('#\[i\](.*?)\[/i\]#si', '<i>$1</i>', $row['message']);
$row['message'] = preg_replace('/\[img\](http:\/\/([a-zA-Z0-9\-\.]+)\.([a-zA-Z]{2,7})\/([a-zA-Z0-9\&\?\/\-=+\(\)\[\]\$\.]+)\.(png|gif|jpg|jpeg|bmp|tiff))\[\/img\]/', '<img src="$1"/>', $row['message']);
$row['message'] = preg_replace('/\[url\](http:\/\/([a-zA-Z0-9\-\.]+)\.([a-zA-Z]{2,7})([a-zA-Z0-9_\&\?\/\-=+\(\)\[\]\$\.]*))\[\/url\]/', '<a rel="nofollow" target="_blank" href="/go.php?url=$1">$1</a>', $row['message']);
$row['message'] = preg_replace('/\[url=(http:\/\/([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,7})([a-zA-Z0-9\&\?\/\-=+\(\)\[\]\$\.]*))\]([a-zA-Z0-9�-��-�:_\.\/\-=\s\r]+)\[\/url\]/', '<a rel="nofollow" target="_blank" href="/go.php?url=$1">$5</a>', $row['message']);

$row['video_v'] = substr($row['link'], strpos($row['link'], 'v=') + 2, strpos(substr($row['link'], strpos($row['link'], 'v=') + 2), '&'));
$row['video'] = '<object width="480" height="385"><param name="movie" value="http://www.youtube.com/v/' . $row['video_v'] . '"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/' .$row['video_v'] . '" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="480" height="385"></embed></object> ';
$entry = $row;
$months = array();
$cat_query = mysql_query("SELECT * FROM `blog` ORDER BY id DESC");
while($cat_row = mysql_fetch_assoc($cat_query)){
	if(array_search($cat_row['year'] . '.' . $cat_row['month'], $months) === false) {
		$months[] = $cat_row['year'] . '.' . $cat_row['month'];
		$months_col[$cat_row['year'] . '.' . $cat_row['month']] = 1;
	} else $months_col[$cat_row['year'] . '.' . $cat_row['month']]++;
};
$russian_months = array('january' => '������', 'february' => '�������', 'march' => '����', 'april' => '������', 'may' => '���', 'june' => '����', 'july' => '����', 'august' => '������', 'september' => '��������', 'october' => '�������', 'november' => '������', 'december' => '�������');
$modified_months = array();
foreach($months as $value)
	$modified_months[] = array(
		'title' => $russian_months[substr($value, 5)] . ' ' . substr($value, 0, 4),
		'col' => $months_col[$value],
		'url' => '/blog/' . substr($value, 0, 4) . '/' . substr($value, 5)
	);
$smarty -> assign('list', array(
	'categories' => $modified_months,
	'e' => $entry,
	'action' => $_GET['act']
));

$smarty -> assign('title', $site['options']['static_pages']['view'] . ' �' . $_GET['id'] . ' - ' . $site['name']);

$smarty -> display('static.tpl');
?>