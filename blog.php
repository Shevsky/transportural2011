<?php
$script_content = 1;
$script = 'blog';
$script_uri = 'blog';

include('params.php');

$nums = $site['options']['show_on_page_blog'];
if(isset($_GET['page'])) {
	$page = intval($_GET['page']);
} else {
	$page = 1;
};

$sql = mysql_query("SELECT COUNT(*) AS `counter` FROM `blog`" . ($_GET['year'] ? ($_GET['month'] ? "WHERE year = '" . $_GET['year'] . "' and month = '" . $_GET['month'] . "'" : "WHERE year = '" . $_GET['year'] . "'") : ""));
notLoggedError(!mysql_num_rows($sql));
$row = mysql_fetch_assoc($sql);
$elements = $row['counter'];

$pages = ceil($elements/$nums);
if($page < 1) {
	$page = 1;
} elseif($page > $pages) {
	$page = $pages;
};
$start = ($page-1) * $nums;
if ($start < 0) $start = 0;
$sql = mysql_query("SELECT * FROM `blog` " . ($_GET['year'] ? ($_GET['month'] ? "WHERE year = '" . $_GET['year'] . "' and month = '" . $_GET['month'] . "'" : "WHERE year = '" . $_GET['year'] . "'") : "") . " ORDER BY id DESC LIMIT {$start}, {$nums}");
$entries = array();
$paginator;

$months = array();
while($row = mysql_fetch_assoc($sql)) {
	$row['date'] = ruDate($row['time']);
	$row['url'] = array(
		'view' => '/blog/' . $row['id'],
		'edit' => '/blog/edit/' . $row['id'],
		'delete' => 'javascript:blog.del(' . $row['id'] . ');'
	);
	$row['message'] = strpos($row['message'], '$CUT$') !== false ? (substr($row['message'], 0, strpos($row['message'], '$CUT$')) . '<br><a href="/blog/' . $row['id'] . '">������ ����� &rarr;</a>') : $row['message'];
	$row['message'] = preg_replace('/\n/', '<br>', $row['message']);
	$row['message'] = preg_replace('#\[b\](.*?)\[/b\]#si', '<b>$1</b>', $row['message']);
	$row['message'] = preg_replace('#\[i\](.*?)\[/i\]#si', '<i>$1</i>', $row['message']);
	$row['message'] = preg_replace('/\[img\](http:\/\/([a-zA-Z0-9\-\.]+)\.([a-zA-Z]{2,7})\/([a-zA-Z0-9\&\?\/\-=+\(\)\[\]\$\.]+)\.(png|gif|jpg|jpeg|bmp|tiff))\[\/img\]/', '<img src="$1"/>', $row['message']);
	$row['message'] = preg_replace('/\[url\](http:\/\/([a-zA-Z0-9\-\.]+)\.([a-zA-Z]{2,7})([a-zA-Z0-9_\&\?\/\-=+\(\)\[\]\$\.]*))\[\/url\]/', '<a rel="nofollow" target="_blank" href="/go.php?url=$1">$1</a>', $row['message']);
	$row['message'] = preg_replace('/\[url=(http:\/\/([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,7})([a-zA-Z0-9\&\?\/\-=+\(\)\[\]\$\.]*))\]([a-zA-Z0-9�-��-�:_\.\/\-=\s\r]+)\[\/url\]/', '<a rel="nofollow" target="_blank" href="/go.php?url=$1">$5</a>', $row['message']);

	$row['video_v'] = substr($row['link'], strpos($row['link'], 'v=') + 2, strpos(substr($row['link'], strpos($row['link'], 'v=') + 2), '&'));
	$row['video'] = '<object width="480" height="385"><param name="movie" value="http://www.youtube.com/v/' . $row['video_v'] . '"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/' . $row['video_v'] . '" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="480" height="385"></embed></object> ';
	$entries[] = $row;
};
$cat_query = mysql_query("SELECT * FROM `blog` ORDER BY id DESC");
while($cat_row = mysql_fetch_assoc($cat_query)){
	if(array_search($cat_row['year'] . '.' . $cat_row['month'], $months) === false) {
		$months[] = $cat_row['year'] . '.' . $cat_row['month'];
		$months_col[$cat_row['year'] . '.' . $cat_row['month']] = 1;
	} else $months_col[$cat_row['year'] . '.' . $cat_row['month']]++;
};
$russian_months = array('january' => '������', 'february' => '�������', 'march' => '����', 'april' => '������', 'may' => '���', 'june' => '����', 'july' => '����', 'august' => '������', 'september' => '��������', 'october' => '�������', 'november' => '������', 'december' => '�������');
$modified_months = array();
foreach($months as $value)
	$modified_months[] = array(
		'title' => $russian_months[substr($value, 5)] . ' ' . substr($value, 0, 4),
		'col' => $months_col[$value],
		'url' => '/blog/' . substr($value, 0, 4) . '/' . substr($value, 5)
	);
$neighbours = 3;
$left_neighbour = $page - $neighbours;
if ($left_neighbour < 1) $left_neighbour = 1;
$right_neighbour = $page + $neighbours;
if ($right_neighbour > $pages) $right_neighbour = $pages;
if ($page > 1) {
	$paginator .= '<a href="/blog/page/' . ($page-1) . '">&larr; �����</a>';
};
for ($i=$left_neighbour; $i<=$right_neighbour; $i++) {
	if ($i != $page) {
		$paginator .= '<a href="/blog/page/' . $i . '">' . $i . '</a>';
	} else {
		$paginator .= '<strong>' . $i . '</strong>';
	};
};
if ($page < $pages) {
	$paginator .= '<a href="/blog/page/' . ($page+1) . '">������ &rarr;</a>';
}

$smarty -> assign('list', array(
	'categories' => $modified_months,
	'folder' => ($_GET['year'] ? ($_GET['month'] ? ($russian_months[$_GET['month']] . ' ' . $_GET['year']) : $_GET['year']) : false),
	'pages' => $pages,
	'entries' => $entries,
	'paginator' => $paginator,
	'action' => $_GET['act'],
	'url' => array(
		'add' => '/blog/add'
	)
));

$smarty -> assign('title', ($_GET['year'] ? ($_GET['month'] ? ($russian_months[$_GET['month']] . ' ' . $_GET['year'] . ' - ') : $_GET['year'] . ' - ') : '') . '��������� - ' . $site['name']);

$smarty -> display('static.tpl');
?>