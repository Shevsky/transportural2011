<?php
/*
 * Codi - ����� ��� �������� �������� ����� �� PHP
 *
 * ��� ��������������� ����� ����������� � �������������� ���� � ������
 * �������� ���������� ��� �������������� ����� - � ���� ������
 * ���������� ��� ������������ �� 1 ���(date('d.m.H'))
 * ��� �������������, ����� �������� ��� ������ �� ������� ���
 */

session_start();

class Codi {

	/*
	 * draw - ������ �������� �����
	 *
	 * $path - ���� � ������� ������������ � ������� �� �������
	 * $backgrounds - ������ � �������� �������������
	 * $font - ��� ������(ttf)
	 * $size - ������ ������
	 * $length - ���������� ���� � �����, �� ��������� - 6
	 * $red, $green, $blue = ���� ������ � ������������ � ���������� �������� ������� RGB, �� ��������� - ������������ �������������
	 */

	public function draw($path, $backgrounds, $font, $size, $length = 6, $red = null, $green = null, $blue = null) {
		header('Content-Type: image/png');
		$session;
		for($i = 0; $i < $length; $i++) $code[] = rand($i ? 0 : 1, 9);
		$background = $path . $backgrounds[rand(0, sizeof($backgrounds) - 1)];
		$image = imagecreatefrompng($background);
		$sizes = getimagesize($background);

		$i = rand((-($size * $length - $sizes[0])) / 2,(-($size * $length - $sizes[0])) / 2 + $size / 1.5);
		foreach($code as $value){
			$session .= $value;
			$color = imagecolorallocate($image, $red ? $red : rand(0, 200), $green ? $green : rand(0, 200), $blue ? $blue : rand(0, 200));
			imagettftext($image, $size, rand( -4, 4), $i, rand(($sizes[1] + $size) / 2,($sizes[1] + $size) / 3), $color, $path . $font, $value);	
			$i = $i +(rand($size - 4, $size - 2));
		};

		$_SESSION['codi_capcha_' . md5(date('d.m.H'))] = md5(md5($session));
		imagepng($image);
		imagedestroy($image);
	}

	/*
	 * session - �������� ��� ������� ������
	 */

	public function session() {
		return 'codi_capcha_' . md5(date('d.m.H'));
	}

	/*
	 * check - ��������� �����
	 *
	 * $code - ��� � ���� ������
	 * $session - ��� ������, �� ��������� - �������
	 */

	public function check($code, $session = null) {
		if(!$session) $session = 'codi_capcha_' . md5(date('d.m.H'));
		if($_SESSION[$session] == md5(md5($code))) return true;
		else return false;
	}

	/*
	 * destroy - ��������� ������ �����, ������������� ������������ ����� ������ ��������(check)
	 *
	 * $session - ��� ������, �� ��������� - �������
	 */

	public function destroy($session = null) {
		if(!$session) $session = md5(date('d.m.H'));
		session_unset($_SESSION['codi_capcha_' . $session]);
	}
};
?>