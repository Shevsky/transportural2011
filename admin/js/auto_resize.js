var FormUtil = FormUtil || {} ;

FormUtil.TextArea = FormUtil.TextArea || {} ;



/* config */

FormUtil.TextArea.conf = {

	resizeClass: "auto-resize",

	resizeMin: 1,

	resizeMax: 30

};



/* resize func */

FormUtil.TextArea.autoResize = function(e){

	var conf = FormUtil.TextArea.conf;

	var textarea = e.target || e.srcElement || e;

	var rowCount = textarea.value.split("\n").length;

	rowCount = Math.min(conf.resizeMax, Math.max(conf.resizeMin, rowCount));

	textarea.setAttribute("rows", rowCount);

};



/* init event */

FormUtil.TextArea.init = function(e){

	var textarea = document.getElementsByTagName("textarea");

	var className = FormUtil.TextArea.conf.resizeClass;

	var regex = new RegExp("(^|\\s+)" + className + "($|\\s+)");

	for(var i=0,max=textarea.length;i<max;i++){

		var tmp = textarea.item(i);

		if(!(tmp.getAttribute("class") || tmp.getAttribute("className") || "").match(regex)) continue;

		if(window.addEventListener){

			tmp.addEventListener("keyup", FormUtil.TextArea.autoResize, false);

		} else if (window.attachEvent) {

			tmp.attachEvent("onkeyup", FormUtil.TextArea.autoResize);

		}

		FormUtil.TextArea.autoResize(tmp);

	}

}



/* onload event */

if (window.addEventListener) {

	window.addEventListener("load", FormUtil.TextArea.init, false);

} else if (window.attachEvent) {

	window.attachEvent("onload", FormUtil.TextArea.init);

}
