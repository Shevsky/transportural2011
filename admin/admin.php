<?php
//date_default_timezone_set('Asia/Yekaterinburg');
session_start();
if($_SERVER['REQUEST_METHOD'] == 'POST' && $_SESSION['group'] == 1) {
header("Content-type: text/html; charset=windows-1251");
	if($_GET['act'] == 'templates') {
		file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/templates/' . $_POST['template'], iconv('utf-8', 'windows-1251', $_POST['code']));
		if(!$_GET['ajax']) header("Location: /admin/templates?id=" . $_POST['id'] . '&save=1');
		else echo 1;
		exit;
	};
};

include('../configs/mysql.php');
$con = mysql_connect($mysql['host'], $mysql['user'], $mysql['password']);
mysql_select_db($mysql['db'], $con);

function conv($string, $method = true){
	if(gettype($string === 'string')) return iconv($method ? 'utf-8' : 'windows-1251', $method ? 'windows-1251' : 'utf-8', $string);
	else return $string;
};
function arriconv($array, $method = true){
	if (is_array($array) || is_object($array)){
		foreach ($array as &$value){
			$value = arriconv($value, $method);
		};
		return $array;
	} else return conv($array, $method);
};

$lang = file_get_contents('lang.dat');
$lang = json_decode(conv($lang, false), true);
$lang = arriconv($lang, true);

if($_SESSION['group'] != 1) {
	header("HTTP/1.1 404 Not Found");
	header("Status: 404 Not Found");
	echo file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/404.php');
	exit;
};
?>