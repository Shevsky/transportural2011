<?php include('admin.php');?>
<?php
$options = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/configs/options.dat');
$options = json_decode(conv($options, false), true);
$options = arriconv($options, true);

if($_SERVER['REQUEST_METHOD'] == 'POST'){
	foreach($_POST as $key => $value)
		$options[$key] = (substr($key, 0, 7) != 'show_on') ? preg_split('/\r\n/', $value) : $value;
	$options['active'] = $_POST['active'] ? 1 : 0;
	$options = arriconv($options, false);

	file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/configs/options.dat', json_encode($options));
	header("Location: /admin/settings.php");
	exit;
};
?>
<!DOCTYPE html PUBLIC  "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml"> 
  <head> 
    <title>���������</title> 
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
	<link rel="StyleSheet" type="text/css" href="./style.css" />
	<link rel="stylesheet" href="./js/codemirror/lib/codemirror.css"> 
    <script src="./js/codemirror/lib/codemirror.js"></script> 
    <link rel="stylesheet" href="./js/codemirror/theme/default.css"> 
    <script src="./js/codemirror/mode/css/css.js"></script>
    <script src="./js/auto_resize.js"></script>
	<link href="./img/favicon.ico" rel="shortcut icon" type="image/x-icon" /> 
  </head> 
  <body>
    <div class="header">
	  <div class="logo"><img src="./img/logo.png" alt="transportural" /><span>������ ����������</span></div>
	  <div class="bar">
	    <ul class="menu">
		  <li><a href="/">������� �� ����</a></li>
		  <li><a href="/admin/users.php">������������</a></li>
		  <li><a href="/admin/templates.php">�������</a></li>
		  <li><a href="/admin/pages.php">��������</a></li>
		  <li><a href="/admin/forum.php">�����</a></li>
		  <li><a href="/admin/ads.php">�������</a></li>
		  <li class="active"><a href="/admin/settings.php">���������</a></li>
		</ul>
	  </div>
	</div>
	<div class="wrapper">
	  <h1>���������</h1>
	  <p>� ���� ������� ����� ��������������� ���������� ��������� ���������� �����. ������ ����� ������ ���� �������� � ����� ������.</p>
	  <form action="/admin/settings.php" method="post">
	    <div class="fr">
		  <label for="type_faces">���� ����<label><br />
		  <textarea name="type_faces" id="type_faces" class="auto-resize"><?php
			foreach($options['type_faces'] as $key => $value) echo $value . (($key  + 1) != count($options['type_faces']) ? '
' : '');
		  ?></textarea>
		</div>
	    <div class="fr">
		  <label for="board_categories">��������� ����� ����������<label><br />
		  <textarea name="board_categories" id="board_categories" class="auto-resize"><?php
			foreach($options['board_categories'] as $key => $value) echo $value . (($key  + 1) != count($options['board_categories']) ? '
' : '');
		  ?></textarea>
		</div>
	    <div class="fr">
		  <label for="load_capacity">����������������<label><br />
		  <textarea name="gruzopod" id="load_capacity" class="auto-resize"><?php
			foreach($options['gruzopod'] as $key => $value) echo $value . (($key  + 1) != count($options['gruzopod']) ? '
' : '');
		  ?></textarea>
		</div>
		<div class="fr">
		  <label for="type">�����<label><br />
		  <textarea name="kuzov" id="type" class="auto-resize"><?php
			foreach($options['kuzov'] as $key => $value) echo $value . (($key  + 1) != count($options['kuzov']) ? '
' : '');
		  ?></textarea>
		</div>
		<div class="fr">
		  <label for="loading">��������<label><br />
		  <textarea name="pogruzka" id="loading" class="auto-resize"><?php
			foreach($options['pogruzka'] as $key => $value) echo $value . (($key  + 1) != count($options['pogruzka']) ? '
' : '');
		  ?></textarea>
		</div>
		<div class="fr">
		  <label for="payment">������<label><br />
		  <textarea name="oplata" id="payment" class="auto-resize"><?php
			foreach($options['oplata'] as $key => $value) echo $value . (($key  + 1) != count($options['oplata']) ? '
' : '');
		  ?></textarea>
		</div>
		<p style="margin-top:10px;">�� ������ ��������� ������ �� ���� ������ ��� �������������� �� ��� ������ �����������. ��� ������� ���������� ������ � ������� �������� ������� �� ��������� ����� ��� ���������� ���������.</p>
		<div class="fr checkBlock">
			    <input class="checkbox" type="checkbox" value="1" id="active" name="active"<?=$options['active'] ? ' checked=""' : '';?>> <label for="active">�������� ����?</label>
		    </div>
		<p style="margin-top:10px;">����� �� ������ �������� ���������� ��������� ���������� �� ����� �������� � ������� ������.</p>
		<div class="fr">
		  <label for="show_on_page_requests">���������� ��������� ���������� � ������ "������"<label><br />
		  <input id="show_on_page_requests" name="show_on_page_requests" value="<?=$options['show_on_page_requests'];?>"/>
		</div>
		<div class="fr">
		  <label for="show_on_page_cargo">���������� ��������� ���������� � ������ "������� ����������"<label><br />
		  <input id="show_on_page_cargo" name="show_on_page_cargo" value="<?=$options['show_on_page_cargo'];?>"/>
		</div>
		<div class="fr">
		  <label for="show_on_page_board">���������� ��������� ���������� � ������ "����� ����������"<label><br />
		  <input id="show_on_page_board" name="show_on_page_board" value="<?=$options['show_on_page_board'];?>"/>
		</div>
		<div class="fr">
		  <label for="show_on_page_news">���������� ��������� ���������� � ������ "�������"<label><br />
		  <input id="show_on_page_news" name="show_on_page_news" value="<?=$options['show_on_page_news'];?>"/>
		</div>
		<div class="fr">
		  <label for="show_on_page_blog">���������� ��������� ���������� � ������ "���������"<label><br />
		  <input id="show_on_page_blog" name="show_on_page_blog" value="<?=$options['show_on_page_blog'];?>"/>
		</div>
		<div class="save">
		  <input type="submit" value="���������" />
		</div>
	  </form>
	</div>
  </body>
</html>
