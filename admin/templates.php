<?php include('admin.php');?>
<?php
$statis_variables = array(
	array(
		'{$site.name}',
		'�������� �����'
	),
	array(
		'{$site.url.home}',
		'������ �� ������� �������� �����'
	),
	array(
		'{$site.url.home}',
		'������ �� ������� �������� �����'
	),
	array(
		'{$site.url.login}',
		'������ ��� ����������� �������������'
	),
	array(
		'{$site.url.signup}',
		'������ ����������� �������������'
	),
	array(
		'{$site.url.logout}',
		'������ ��� ������ �������������'
	),
	array(
		'{$site.url.cargo}',
		'������ �� ������� �����������������'
	),
	array(
		'{$site.url.req}',
		'������ �� ������ ������'
	),
	array(
		'{$site.url.board}',
		'������ �� ����� ����������'
	),
	array(
		'{$site.url.search}',
		'������ ��� ������������ ������ �� �����'
	),
	array(
		'{$site.url.active}',
		'������ ��� ��������� �������'
	),
	array(
		'{$site.url.user_profile}',
		'������ �� ������� ������������'
	),
	array(
		'{$site.user.logged}',
		'����, ����������� ����������� �� ������������'
	),
	array(
		'{$site.user.login}',
		'����� ������������, ������������ � ������ ��������'
	),
	array(
		'{$site.user.ulogin}',
		'����� ������������, ������������ � ������ ��������, ������������ � ������ �������'
	),
	array(
		'{$site.user.email}',
		'����������� ����� ������������'
	),
	array(
		'{$site.user.id}',
		'���������� ������������� ������������'
	),
	array(
		'{$site.user.group}',
		'������ ������������ (0 &mdash; ������� ������������, 1 &mdash; �������������)'
	),
	array(
		'{$site.user.active}',
		'����, ����������� ����������� �� ������� ������� ������������'
	)
);
?>
<!DOCTYPE html PUBLIC  "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml"> 
  <head> 
    <title>������� &mdash; ������ ����������</title> 
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
	<link rel="StyleSheet" type="text/css" href="./style.css" />
	<link rel="stylesheet" href="./js/codemirror/lib/codemirror.css"> 
    <script src="./js/codemirror/lib/codemirror.js"></script> 
    <link rel="stylesheet" href="./js/codemirror/theme/default.css"> 
    <script src="./js/codemirror/mode/<?if($_GET['id'] == 3):?>css/css<?else:?>xml/xml<?endif?>.js"></script>
    <script src="/libs/jquery.min.js"></script>
	<script src="./js/auto_resize.js"></script>
	<link href="./img/favicon.ico" rel="shortcut icon" type="image/x-icon" /> 
  </head> 
  <body>
    <div class="header">
	  <div class="logo"><img src="./img/logo.png" alt="transportural" /><span>������ ����������</span></div>
	  <div class="bar">
	    <ul class="menu">
		  <li><a href="/">������� �� ����</a></li>
		  <li><a href="/admin/users.php">������������</a></li>
		  <li class="active"><a href="/admin/templates.php">�������</a></li>
		  <li><a href="/admin/pages.php">��������</a></li>
		  <li><a href="/admin/forum.php">�����</a></li>
		  <li><a href="/admin/ads.php">�������</a></li>
		  <li><a href="/admin/settings.php">���������</a></li>
		</ul>
	  </div>
	</div>
	<div class="wrapper">
	  <div class="sidebar">
		<div class="block">
		  <h1>������ ��������</h1>
		  <ul class="temps">
		    <li><?if($_GET['id'] != 1):?><a href="/admin/templates.php?id=1"><?=$lang['templates'][1][0];?></a><?else:?><?=$lang['templates'][1][0];?><?endif?></li>
		    <li><?if($_GET['id'] != 2):?><a href="/admin/templates.php?id=2"><?=$lang['templates'][2][0];?></a><?else:?><?=$lang['templates'][2][0];?><?endif?></li>
		    <li><?if($_GET['id'] != 3):?><a href="/admin/templates.php?id=3"><?=$lang['templates'][3][0];?></a><?else:?><?=$lang['templates'][3][0];?><?endif?></li>
		  </ul>
		  <ul class="temps">
		    <li><?if($_GET['id'] != 4):?><a href="/admin/templates.php?id=4"><?=$lang['templates'][4][0];?></a><?else:?><?=$lang['templates'][4][0];?><?endif?></li>
		    <li><?if($_GET['id'] != 5):?><a href="/admin/templates.php?id=5"><?=$lang['templates'][5][0];?></a><?else:?><?=$lang['templates'][5][0];?><?endif?></li>
		    <li><?if($_GET['id'] != 6):?><a href="/admin/templates.php?id=6"><?=$lang['templates'][6][0];?></a><?else:?><?=$lang['templates'][6][0];?><?endif?></li>
		  </ul>
		  <ul class="temps">
		    <li><?if($_GET['id'] != 7):?><a href="/admin/templates.php?id=7"><?=$lang['templates'][7][0];?></a><?else:?><?=$lang['templates'][7][0];?><?endif?></li>
		    <li><?if($_GET['id'] != 8):?><a href="/admin/templates.php?id=8"><?=$lang['templates'][8][0];?></a><?else:?><?=$lang['templates'][8][0];?><?endif?></li>
		  </ul>
		  <ul class="temps">
		    <li><?if($_GET['id'] != 9):?><a href="/admin/templates.php?id=9"><?=$lang['templates'][9][0];?></a><?else:?><?=$lang['templates'][9][0];?><?endif?></li>
		    <li><?if($_GET['id'] != 10):?><a href="/admin/templates.php?id=10"><?=$lang['templates'][10][0];?></a><?else:?><?=$lang['templates'][10][0];?><?endif?></li>
		  </ul>
		  <ul class="temps">
		    <li><?if($_GET['id'] != 11):?><a href="/admin/templates.php?id=11"><?=$lang['templates'][11][0];?></a><?else:?><?=$lang['templates'][11][0];?><?endif?></li>
		    <li><?if($_GET['id'] != 12):?><a href="/admin/templates.php?id=12"><?=$lang['templates'][12][0];?></a><?else:?><?=$lang['templates'][12][0];?><?endif?></li>
		    <li><?if($_GET['id'] != 13):?><a href="/admin/templates.php?id=13"><?=$lang['templates'][13][0];?></a><?else:?><?=$lang['templates'][13][0];?><?endif?></li>
		    <li><?if($_GET['id'] != 14):?><a href="/admin/templates.php?id=14"><?=$lang['templates'][14][0];?></a><?else:?><?=$lang['templates'][14][0];?><?endif?></li>
		  </ul>
		  <ul class="temps">
		    <li class="tempsHead"><strong>����������������</strong></li>
		    <li><?if($_GET['id'] != 15):?><a href="/admin/templates.php?id=15"><?=$lang['templates'][15][0];?></a><?else:?><?=$lang['templates'][15][0];?><?endif?></li>
		    <li><?if($_GET['id'] != 16):?><a href="/admin/templates.php?id=16"><?=$lang['templates'][16][0];?></a><?else:?><?=$lang['templates'][16][0];?><?endif?></li>
		    <li><?if($_GET['id'] != 17):?><a href="/admin/templates.php?id=17"><?=$lang['templates'][17][0];?></a><?else:?><?=$lang['templates'][17][0];?><?endif?></li>
		    <li><?if($_GET['id'] != 18):?><a href="/admin/templates.php?id=18"><?=$lang['templates'][18][0];?></a><?else:?><?=$lang['templates'][18][0];?><?endif?></li>
		  </ul>
		  <ul class="temps">
		    <li class="tempsHead"><strong>������</strong></li>
		    <li><?if($_GET['id'] != 19):?><a href="/admin/templates.php?id=19"><?=$lang['templates'][15][0];?></a><?else:?><?=$lang['templates'][15][0];?><?endif?></li>
		    <li><?if($_GET['id'] != 20):?><a href="/admin/templates.php?id=20"><?=$lang['templates'][16][0];?></a><?else:?><?=$lang['templates'][16][0];?><?endif?></li>
		    <li><?if($_GET['id'] != 21):?><a href="/admin/templates.php?id=21"><?=$lang['templates'][17][0];?></a><?else:?><?=$lang['templates'][17][0];?><?endif?></li>
		    <li><?if($_GET['id'] != 22):?><a href="/admin/templates.php?id=22"><?=$lang['templates'][18][0];?></a><?else:?><?=$lang['templates'][18][0];?><?endif?></li>
		  </ul>
		  <ul class="temps">
		    <li class="tempsHead"><strong>����� ����������</strong></li>
		    <li><?if($_GET['id'] != 23):?><a href="/admin/templates.php?id=23"><?=$lang['templates'][15][0];?></a><?else:?><?=$lang['templates'][15][0];?><?endif?></li>
		    <li><?if($_GET['id'] != 24):?><a href="/admin/templates.php?id=24"><?=$lang['templates'][16][0];?></a><?else:?><?=$lang['templates'][16][0];?><?endif?></li>
		    <li><?if($_GET['id'] != 25):?><a href="/admin/templates.php?id=25"><?=$lang['templates'][17][0];?></a><?else:?><?=$lang['templates'][17][0];?><?endif?></li>
		    <li><?if($_GET['id'] != 26):?><a href="/admin/templates.php?id=26"><?=$lang['templates'][18][0];?></a><?else:?><?=$lang['templates'][18][0];?><?endif?></li>
		  </ul>
		  <ul class="temps">
		    <li class="tempsHead"><strong>���������</strong></li>
		    <li><?if($_GET['id'] != 27):?><a href="/admin/templates.php?id=27"><?=$lang['templates'][15][0];?></a><?else:?><?=$lang['templates'][15][0];?><?endif?></li>
		    <li><?if($_GET['id'] != 28):?><a href="/admin/templates.php?id=28"><?=$lang['templates'][16][0];?></a><?else:?><?=$lang['templates'][16][0];?><?endif?></li>
		    <li><?if($_GET['id'] != 29):?><a href="/admin/templates.php?id=29"><?=$lang['templates'][17][0];?></a><?else:?><?=$lang['templates'][17][0];?><?endif?></li>
		    <li><?if($_GET['id'] != 30):?><a href="/admin/templates.php?id=30"><?=$lang['templates'][18][0];?></a><?else:?><?=$lang['templates'][18][0];?><?endif?></li>
		  </ul>
		  <ul class="temps">
		    <li class="tempsHead"><strong>�������</strong></li>
		    <li><?if($_GET['id'] != 31):?><a href="/admin/templates.php?id=31"><?=$lang['templates'][15][0];?></a><?else:?><?=$lang['templates'][15][0];?><?endif?></li>
		    <li><?if($_GET['id'] != 32):?><a href="/admin/templates.php?id=32"><?=$lang['templates'][16][0];?></a><?else:?><?=$lang['templates'][16][0];?><?endif?></li>
		    <li><?if($_GET['id'] != 33):?><a href="/admin/templates.php?id=33"><?=$lang['templates'][17][0];?></a><?else:?><?=$lang['templates'][17][0];?><?endif?></li>
		    <li><?if($_GET['id'] != 34):?><a href="/admin/templates.php?id=34"><?=$lang['templates'][18][0];?></a><?else:?><?=$lang['templates'][18][0];?><?endif?></li>
		  </ul>
		</div>
      </div>
	  <div class="content">
		<?if(isset($_GET['id'])):?>
		<h1><?=$lang['templates'][$_GET['id']][0];?></h1>
		<!--<form onsubmit="$('#status_ok').hide();$('#status_loading').show();$.post('/admin/admin.php?act=templates&ajax=1&id=<?=$_GET['id'];?>', $(this).serialize(), function(data){if(data == 1) {$('#status_ok').show();$('#status_loading').hide();} else alert(data);});return false;" action="/admin/admin.php?act=templates&id=<?=$_GET['id'];?>" method="post">
		<input type="hidden" name="template" value="<?=$lang['templates'][$_GET['id']][1];?>"/>
		<input type="hidden" name="id" value="<?=$_GET['id'];?>"/>
		<textarea name="code" class="tmlForm" id="code"><?php
			//echo htmlspecialchars(file_get_contents('../templates/' . $lang['templates'][$_GET['id']][1]));
		?></textarea>
		<div class="tmlSubmit"><input type="submit" value="��������� ������" /></div>
		<div id="status_ok" style="color:green;text-align:center;padding-top:5px;<?if($_GET['save']):?>display:block;<?else:?>display:none;<?endif?>">
			���������
		</div>
		<div id="status_loading" style="color:#555;text-align:center;padding-top:5px;display:none;">
			���������
		</div>
		</form>-->
		<?if($_GET['id'] != 3):?>
		<div class="codeTop"><span>�������� ����������</span></div>
		
		<div class="codeDescr">
		<?
			if($lang['templates'][$_GET['id']][2] != 'static')
				foreach($lang['templates'][$_GET['id']][2] as $value) {
					echo '<div class="code"><span>' . $value[0] . '</span> &mdash; ' . $value[1] . '</div>
					';
				};
			echo '<div class="br"></div>';
			foreach($statis_variables as $value) {
				echo '<div class="code"><span>' . $value[0] . '</span> &mdash; ' . $value[1] . '</div>
				';
			};
		?>
		<div class="br"></div>
			<div class="code"><span>{if <b>condition</b>} True {else} False {/if}</span> &mdash; �������� ���������</div>
			<div class="code"><span>{section name = <b>param1</b>, loop = <b>param2</b>[, step = <b>param2</b>]} Content {sectionelse} No content! {/section}</span> &mdash; ����</div>
			<div class="code"><span>{literal} function foo(){return bar;} {/literal}</span> &mdash; ��������, ������ �������� �� �������������� �������� ������</div>
		</div>
		
		<div class="codeBottom"><span>/�������� ����������</span></div>	
		<?endif?>
		<?endif?>
	  </div>
	</div>
  </body>
</html>