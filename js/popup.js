function popup(name, text){
	if(name == 'close') $('#popup').fadeOut('fast');
	else {
		$('#popupTitle').html(name);
		$('#popupText').html(text);
		$('#popup').fadeIn('fast');
	};
};