<?php
$script_content = 1;
$script = 'board.panel';
$script_uri = 'board';

include('params.php');

notLoggedError(!$_SESSION['logged']);

if($_GET['method'] == 'post' && $_GET['act'] == 'add' && $_SESSION['logged'] == 1) {
	if($_POST['description'] && $_POST['title']) {
		require('libs/uploader.class.php');
		$n;
		$i;
		$o = 1;
		foreach($_FILES as $value) if($value['size'] > 0) {
			$n .= ", `photo".$o."`";
			$i .= ", 1";
			$o++;
		};
		$query = mysql_query("INSERT INTO `board` (`category`, `uid`, `title`, `description`, `time`".$n.") VALUES (
		'".$_POST['category']."', '".$_SESSION['id']."', '".$_POST['title']."', '".htmlspecialchars($_POST['description'])."', '".time()."'".$i.")", $con);
		$_id = mysql_insert_id();
		$o = 1;
		foreach($_FILES as $key => $value){
			if($value['size'] > 0) {
				if ($value['type'] == 'image/jpeg')
					$source = imagecreatefromjpeg($value['tmp_name']);
				elseif ($value['type'] == 'image/png')
					$source = imagecreatefrompng($value['tmp_name']);
				elseif ($value['type'] == 'image/gif')
					$source = imagecreatefromgif($value['tmp_name']);
				else {
					echo '�������������� ������� ����������� - jpeg, png � gif. ��� ������ - ' . $value['type'] . ', ���������� �������� �������������� � ����������� ������ ������� � �������� �� ����� ��������������. � ���� ��������� � ���������� ��������� ���������� ������� �������.';
					mysql_query("DELETE FROM `board` WHERE `id` = '" . $_id . "'");
					exit;
				};
				$width = imagesx($source);
				$uploader = new uploader();
				$uploader->source = $value;
				$uploader->autoName = md5('photo-' . $_id . '-' . $o);
				if($width > 1024) {
					$uploader->destDir = $site['root'] . '/temp/';
					$uploader->resizeDir = $site['root'] . '/upload/';
					$uploader->resize($uploader->upload(''),1024,1280);
					unlink($site['root'] . '/temp/' . md5('photo-' . $_id . '-' . $o) . '.jpg');
				} else {
					$uploader->destDir = $site['root'] . '/upload/';
					$uploader->upload('');
				};
				$uploader->newWidth = 100;
				$uploader->newHeight = 100;
				$uploader->resizeDir = $site['root'] . '/upload/r/';
				$uploader->resize();
				$o++;
			};
		};
		require('notificator.php');
		$notificator = new notificator();
		$notificator::send('board', array('id' => $_id, 'title' => $_POST['title']));
		if($_id) header('Location: /board/'.$_id);
		exit;
	};
} elseif($_GET['method'] == 'post' && $_GET['act'] == 'edit') {
	require('libs/uploader.class.php');
	$sql = mysql_query("SELECT * FROM `board` WHERE id = '" . $_GET['id'] . "' ORDER BY id DESC");
	$row = mysql_fetch_assoc($sql);
	if($_SESSION['group'] == 1 || $_SESSION['id'] == $row['uid']){
	$toRemove = array();
	
	for($i = 1; $i <= 10; $i++){
		$photosActions[$i] = $row['photo'.$i] * 1;
		if($_POST['photo'.$i.'_remove']) {
			$photosActions[$i] = 0;
		};
	};
	foreach($_FILES as $key => $value){
		if($value['size'] > 0) {
		if ($value['type'] == 'image/jpeg')
			$source = imagecreatefromjpeg($value['tmp_name']);
		elseif ($value['type'] == 'image/png')
			$source = imagecreatefrompng($value['tmp_name']);
		elseif ($value['type'] == 'image/gif')
			$source = imagecreatefromgif($value['tmp_name']);
			$rand_mix = rand(5, 999);
			$width = imagesx($source);
			$uploader = new uploader();
			$uploader->source = $value;
			$uploader->autoName = md5('photo-' . $row['id'] . '-' . substr($key, 5) . '-' . $rand_mix);
			if($width > 1024) {
				$uploader->destDir = $site['root'] . '/temp/';
				$uploader->resizeDir = $site['root'] . '/upload/';
				$uploader->resize($uploader->upload(''),1024,1280);
				unlink($site['root'] . '/temp/' . md5('photo-' . $row['id'] . '-' . substr($key, 5) . '-' . $rand_mix) . '.jpg');
			} else {
				$uploader->destDir = $site['root'] . '/upload/';
				$uploader->upload('');
			};
			$uploader->newWidth = 100;
			$uploader->newHeight = 100;
			$uploader->resizeDir = $site['root'] . '/upload/r/';
			$uploader->resize();
			$photosActions[substr($key, 5) * 1] = $rand_mix;
		};
	};
	$l = '';
	foreach($photosActions as $key => $value){
		$l .= ', `photo'.$key.'` = ' . $value;
	};
	$query = mysql_query("UPDATE board SET `category` = '".$_POST['category']."', `title` = '".$_POST['title']."', `description` = '".$_POST['description']."'".$l." WHERE id=".$_GET['id'], $con);
	header('Location: /board/'.$_GET['id']);
	} else header('Location: /');
} elseif($_GET['act'] == 'add') {
	$smarty -> assign('action', 'add');
	$smarty -> assign('list', array(
		'type' => $_GET['type'],
		'url' => array(
			'post' => '/board/add/post'
		)
	));
} elseif($_GET['act'] == 'edit'){
	$sql = mysql_query("SELECT * FROM `board` WHERE id = '" . $_GET['id'] . "' ORDER BY id DESC");
	if(mysql_num_rows($sql) == 1){
		$row = mysql_fetch_assoc($sql);
		if($row['uid'] == $_SESSION['id'] || $_SESSION['group'] == 1){
				$row['url'] = array(
					'edit' => '/board/edit/' . $row['id'],
					'delete' => 'javascript:board.del(' . $row['id'] . ');',
				);
				for($i = 0; $i <= 9; $i++) {
					if($row['photo' . ($i + 1)]) {
						$row['photo'][$i] = array(
							 '/upload/'.md5('photo-' . $row['id'] . '-' . ($i + 1) . ($row['photo' . ($i + 1)] >= 5 ? '-' . $row['photo' . ($i + 1)] : '')).'.jpg',
							 '/upload/r/'.md5('photo-' . $row['id'] . '-' . ($i + 1) . ($row['photo' . ($i + 1)] >= 5 ? '-' . $row['photo' . ($i + 1)] : '')) . '.jpg'
						);
					};
				};
				$entries = $row;
			$smarty -> assign('action', 'edit');
			$smarty -> assign('list', array(
				'type' => $_GET['type'],
				'e' => $entries,
				'url' => array(
					'post' => '/board/edit/' . $_GET['id'] . '/post'
				)
			));
		} else notLoggedError(true);
	} else notLoggedError(true);
};

$smarty -> assign('title', ($_GET['act'] == 'add' ? $site['options']['static_pages']['board_panel_add'] : $row['title']) . ' - ' . $site['name']);

$smarty -> display('static.tpl');
?>