<?php
//date_default_timezone_set('Asia/Yekaterinburg');
if(!$header) header('Content-type: text/html; charset=windows-1251'); // ���������

// ������� ������
session_start();
// ������ �������� �������
$transportural = file_get_contents('http://stufford.ru/transportural.php');
if($transportural === 'false') exit;
function conv($string, $method = true){
	if(gettype($string === 'string')) return iconv($method ? 'utf-8' : 'windows-1251', $method ? 'windows-1251' : 'utf-8', $string);
	else return $string;
};
function arriconv($array, $method = true){
	if (is_array($array) || is_object($array)){
		foreach ($array as &$value){
			$value = arriconv($value, $method);
		};
		return $array;
	} else return conv($array, $method);
};
function udd($string){
	return preg_replace("/\'/", "\\'", urldecode($string));
};
function declension($digit,$expr,$onlyword=false) {
        if(!is_array($expr)) $expr = array_filter(explode(' ', $expr));
        if(empty($expr[2])) $expr[2]=$expr[1];
        $i=preg_replace('/[^0-9]+/s','',$digit)%100;
        if($onlyword) $digit='';
        if($i>=5 && $i<=20) $res=$expr[2];
        else
        {
            $i%=10;
            if($i==1) $res=$expr[0];
            elseif($i>=2 && $i<=4) $res=$expr[1];
            else $res=$expr[2];
        }
        return trim($res);
}

function notLoggedError($condition){
	if($condition) {
		header("HTTP/1.1 404 Not Found");
		header("Status: 404 Not Found");
		echo file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/404.php');
		exit;
	};
};
function ruDate($b = 0) {
  if(!$b) $b = time();
  $n = array(
  0,  '������', '�������', '�����', '������', '���', '����', '����', '�������', '��������', '�������', '������', '�������'
  );
  return date('j ', $b).$n[(int)date('m', $b)].date(' Y', $b);
};
function russian_date() {
   $translation = array(
      "am" => "��",
      "pm" => "��",
      "AM" => "��",
      "PM" => "��",
      "Monday" => "�����������",
      "Mon" => "��",
      "Tuesday" => "�������",
      "Tue" => "��",
      "Wednesday" => "�����",
      "Wed" => "��",
      "Thursday" => "�������",
      "Thu" => "��",
      "Friday" => "�������",
      "Fri" => "��",
      "Saturday" => "�������",
      "Sat" => "��",
      "Sunday" => "�����������",
      "Sun" => "��",
      "January" => "������",
      "Jan" => "���",
      "February" => "�������",
      "Feb" => "���",
      "March" => "�����",
      "Mar" => "���",
      "April" => "������",
      "Apr" => "���",
      "May" => "���",
      "May" => "���",
      "June" => "����",
      "Jun" => "���",
      "July" => "����",
      "Jul" => "���",
      "August" => "�������",
      "Aug" => "���",
      "September" => "��������",
      "Sep" => "���",
      "October" => "�������",
      "Oct" => "���",
      "November" => "������",
      "Nov" => "���",
      "December" => "�������",
      "Dec" => "���",
      "st" => "��",
      "nd" => "��",
      "rd" => "�",
      "th" => "��",
      );
   if (func_num_args() > 1) {
      $timestamp = func_get_arg(1);
      return strtr(date(func_get_arg(0), $timestamp), $translation);
   } else {
      return strtr(date(func_get_arg(0)), $translation);
   };
}

// ��������� ��� �����
$site = array(
	'protocol' => 'http', // ��������, ������� ���������� ����, ����� �� ��������, ��� ���� �� �� ����� ������
	'domain' => 'transportural.ru', // ����� ����� ������ � �����
	'root' => $_SERVER['DOCUMENT_ROOT'],
	'method' => $_SERVER['REQUEST_METHOD'] === 'POST' ? $_POST : $_GET,
	'act' => (($_GET['act'] == 'login' || $_GET['act'] == 'signup') ? 'user' : ($_GET['act'] == 'edit' ? 'static' : 'page')),
	'gact' => $_GET['act'] ? $_GET['act'] : 'index',
	'nact' => $_GET['page'] ? $_GET['page'] : 'index',
	'options' => file_get_contents('configs/options.dat')
);
$site['options'] = json_decode(conv($site['options'], false), true);
$site['options'] = arriconv($site['options'], true);
$site['name'] = $site['options']['name'];

if(!$site['options']['active'] && $_SESSION['group'] != 1 && !$noActive) {
	if($_SESSION['logged']) session_destroy();
	echo file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/admin.login.html');
	exit;
};

if($transportural === 'false') exit;

// ����������� � ��
include('configs/mysql.php');
$con = mysql_connect($mysql['host'], $mysql['user'], $mysql['password']);
mysql_select_db($mysql['db'], $con);

if($script_uri != 'news') {
	$inf_query = mysql_query("SELECT * FROM `news` ORDER BY id DESC LIMIT 1", $con);
	$inf_row = mysql_fetch_assoc($inf_query);
	$inf_row['message'] = preg_replace('/\n/', '<br>', $inf_row['message']);
	$inf_row['message'] = preg_replace('#\[b\](.*?)\[/b\]#si', '<b>$1</b>', $inf_row['message']);
	$inf_row['message'] = preg_replace('#\[i\](.*?)\[/i\]#si', '<i>$1</i>', $inf_row['message']);
	$inf_row['message'] = preg_replace('/\[img\](http:\/\/([a-zA-Z0-9\-\.]+)\.([a-zA-Z]{2,7})\/([a-zA-Z0-9\&\?\/\-=+\(\)\[\]\$\.]+)\.(png|gif|jpg|jpeg|bmp|tiff))\[\/img\]/', '<img src="$1"/>', $inf_row['message']);
	$inf_row['message'] = preg_replace('/\[url\](http:\/\/([a-zA-Z0-9\-\.]+)\.([a-zA-Z]{2,7})([a-zA-Z0-9_\&\?\/\-=+\(\)\[\]\$\.]*))\[\/url\]/', '<a rel="nofollow" target="_blank" href="/go.php?url=$1">$1</a>', $inf_row['message']);
	$inf_row['message'] = preg_replace('/\[url=(http:\/\/([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,7})([a-zA-Z0-9\&\?\/\-=+\(\)\[\]\$\.]*))\]([a-zA-Z0-9�-��-�:_\.\/\-=\s\r]+)\[\/url\]/', '<a rel="nofollow" href="/go.php?url=$1">$5</a>', $inf_row['message']);
	$inf_row['message'] = preg_replace("/\[video\](http:\/\/([w]{3}\.)?youtube\.com\/(.*))\[\/video\]/ies", "get_video_com('\\1')", $inf_row['message']);
	$inf_row['message'] = preg_replace("/\[video\](http:\/\/([w]{3}\.)?youtu\.be\/(.*))\[\/video\]/ies", "get_video_be('\\3')", $inf_row['message']);

};

if($script != 'freighters') {
	$freighters_query = mysql_query("SELECT * FROM `users` ORDER BY `freighter_reputation` DESC LIMIT 0, 5", $con);
	while($freighters_row = mysql_fetch_assoc($freighters_query))
		$freighters[] = $freighters_row;
};

if($script != 'customers') {
	$customers_query = mysql_query("SELECT * FROM `users` ORDER BY `customer_reputation` DESC LIMIT 0, 5", $con);
	while($customers_row = mysql_fetch_assoc($customers_query))
		$customers[] = $customers_row;
};

$banner_query = mysql_query("SELECT * FROM `banners` ORDER BY Rand() LIMIT 1");
$banner = mysql_fetch_assoc($banner_query);
$banner['target'] = substr($banner['url'], 0, strlen('http://transportural.ru')) != 'http://transportural.ru' ? '_blank' : '';
$banner['link'] = substr($banner['url'], 0, strlen('http://transportural.ru')) != 'http://transportural.ru' ? '/go.php?url=' : '';

// ����������� Smarty
require_once('libs/Smarty.class.php');
$smarty = new Smarty();
$smarty -> template_dir = $site['root'] . '/templates/';
$smarty -> compile_dir = $site['root'] . '/templates_c/';
$smarty -> config_dir = $site['root'] . '/configs/';
$smarty -> cache_dir = $site['root'] . '/cache/';
$smarty -> caching = false;

$online_query = mysql_query("SELECT * FROM `users` WHERE " . time() . " - last_entry <= 600");
$online_users = array();
$online_users['col'] = 0;
while($online_row = mysql_fetch_assoc($online_query)){
	$online_users['col']++;
	$online_users[] = $online_row;
};
// ����������� ���������� ��� Smarty
$smarty -> assign('site', array(
	'script' => $script,
	'script_uri' => $script_uri,
	'script_page' => $script_page,
	'name' => $site['name'],
	'url' => array(
		'home' => '/',
		'login' => '/index/login',
		'logout' => '/index/logout',
		'signup' => '/index/signup',
		'cargo' => '/auto',
		'req' => '/',
		'board' => '/board',
		'news' => '/news',
		'blog' => '/blog',
		'search' => '/search.php',
		'mail' => '/mail',
		'forum' => '/forum',
		'user_profile' => $_SESSION['logged'] ? '/user/' . $_SESSION['login'] : ''
	),
	'online' => $online_users,
	'informer' => array(
		'news' => ($script_uri != 'news' ? array(
			'id' => $inf_row['id'],
			'message' => $inf_row['message'],
			'date' => ruDate($inf_row['time']),
			'time' => $inf_row['time']
		) : false),
		'freighters' => ($script != 'freighters' ? $freighters : false),
		'customers' => ($script != 'customers' ? $customers : false),
		'banner' => $banner
	),
	'act' => $site['gact'],
	'action' => $_SERVER['REQUEST_METHOD'] === 'POST' ? 'post' : $site['act'],
	'page' => $_GET['page'] ? $_GET['page'] : (!$_GET['act'] ? 'index' : 'page'),
	'lang' => $site['options']
));
if(isset($_COOKIE['session']) && $_COOKIE['session'] === md5($_COOKIE['email'] . '_AFK_' . $_COOKIE['password'] . '_AMX')) {
	$result = mysql_query("SELECT * FROM users WHERE email = '" . mb_strtolower($_COOKIE['email']) . "' and password = '" . $_COOKIE['password'] . "'", $con);
	$row_cookie = mysql_fetch_assoc($result);
	if(mysql_num_rows($result) > 0 && !$_SESSION['logged']) {
		$_SESSION['logged'] = true;
		foreach($row_cookie as $key => $value) if($key != 'password' && $key != 'login') $_SESSION[$key] = $value;
	};
};
if($_SESSION['logged'] == 1) {
	$new_messages = mysql_query("SELECT * FROM `mail` WHERE `to` = '" . $_SESSION['id'] . "' and `read` = 0");
	$unread_messages = mysql_num_rows($new_messages);
	$user_get_info = mysql_query("SELECT * FROM `users` WHERE `id` = " . $_SESSION['id']);
	$user_get_row = mysql_fetch_assoc($user_get_info);
	if($user_get_row['ban'] != $_SESSION['ban']) $_SESSION['ban'] = $user_get_row['ban'];
	$smarty -> append('site', array(
		'user' => array(
			'logged' => true,
			'login' => $_SESSION['login'],
			'ulogin' => mb_strtolower($_SESSION['login']),
			'imya' => $_SESSION['imya'],
			'familiya' => $_SESSION['familiya'],
			'otchestvo' => $_SESSION['otchestvo'],
			'email' => $_SESSION['email'],
			'date' => $_SESSION['date'],
			'signature' => $_SESSION['signature'],
			'avatar' => $_SESSION['avatar'],
			'phone' => $_SESSION['phone'],
			'id' => $_SESSION['id'],
			'ban' => $_SESSION['ban'],
			'group' => (int)$_SESSION['group'],
			'status' => 'online',
			'userpic' => $site['domain'] ? ($_SESSION['userpic'] ? 'http://' . $site['domain'] . $_SESSION['userpic'] : false) : $_SESSION['userpic']
		),
		'unread_messages' => $unread_messages
	), true);
};
$smarty -> append('site', array(
	'content' => $script_content ? $script : ((($_SERVER['REQUEST_METHOD'] === 'POST' && $script != 'list.panel') || $site['act'] == 'user') ? $site['gact'] : 'page.edit')
), true);
if($_SESSION['login']) $smarty -> assign('session', array(
	'login' => $_SESSION['login']
));
if((time()-$_SESSION['last_entry'] * 1>540) && $_SESSION['logged'] == 1) {
mysql_query("UPDATE users SET `last_entry`='".time()."' WHERE id='".$_SESSION['id']."'");
};
$smartyVars = $smarty -> get_template_vars('site');
?>