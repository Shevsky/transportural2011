<?php
/*
* Smarty plugin
* -------------------------------------------------------------
* File:     resource.fromvar.php
* Type:     resource
* Name:     fromvar
* Purpose:  Fetches templates from a global variable
* Author:   Altex
* -------------------------------------------------------------
*/
function smarty_resource_fromvar_source($tpl_name, &$tpl_source, $smarty)
{
    // do database call here to fetch your template,
    // populating $tpl_source
    if (isset($GLOBALS[$tpl_name])) {
        $tpl_source = $GLOBALS[$tpl_name];
        return true;
    } else {
        return false;
    }
}

function smarty_resource_fromvar_timestamp($tpl_name, &$tpl_timestamp, $smarty)
{
	    $tpl_timestamp = time();
        return true;
}

function smarty_resource_fromvar_secure($tpl_name, &$smarty)
{
    // assume all templates are secure
    return true;
}

function smarty_resource_fromvar_trusted($tpl_name, &$smarty)
{
    // not used for templates
}
?> 