<?php
$script_content = 1;
$script = 'board';
$script_uri = 'board';

include('params.php');

$nums = $site['options']['show_on_page_board'];
if(isset($_GET['page'])) {
	$page = intval($_GET['page']);
} else {
	$page = 1;
};

$sql = mysql_query("SELECT * FROM `board`");
$categories = array();
$i = 0;
function array_search_categories($value, $array, $key){
	$return = false;
	foreach($array as $a => $b){
		if($b[$key] === $value) {
			$return = $a;
			break;
		};
	};
	return $return;
};
while($row = mysql_fetch_assoc($sql)){
	$category_id = array_search($row['category'], $site['options']['board_categories']);
	if(array_search_categories($category_id, $categories, 'id') === false) {
		$categories[$i] = array(
			'active' => ($_GET['category'] * 1 - 1) === $category_id ? 'active' : '',
			'id' => $category_id,
			'name' => $row['category'],
			'col' => 1
		);
		$i++;
	} else $categories[array_search_categories($category_id, $categories, 'id')]['col']++;
};

if(isset($_GET['category']))
	$selectWhere = " WHERE `category` = '" . $site['options']['board_categories'][$_GET['category'] * 1 - 1] . "'";
else $selectWhere = '';

$sql = mysql_query("SELECT COUNT(*) AS `counter` FROM `board`" . $selectWhere);
$row = mysql_fetch_assoc($sql);
$elements = $row['counter'];

$pages = ceil($elements/$nums);
if($page < 1) {
	$page = 1;
} elseif($page > $pages) {
	$page = $pages;
};
$start = ($page-1) * $nums;
if ($start < 0) $start = 0;
$sql = mysql_query("SELECT * FROM `board`" . $selectWhere . " ORDER BY id DESC LIMIT {$start}, {$nums}");
$entries = array();
$paginator;

while($row = mysql_fetch_assoc($sql)) {
	$row['url'] = array(
		'edit' => '/board/edit/' . $row['id'],
		'view' => '/board/' . $row['id'],
		'delete' => 'javascript:board.del(' . $row['id'] . ');',
	);
	$usql = mysql_query("SELECT * FROM `users` WHERE id = " . $row['uid'] . " LIMIT 0, 1");
	$urow = mysql_fetch_assoc($usql);
	$row['author'] = array(
		'familiya' => $urow['familiya'],
		'imya' => $urow['imya'],
		'otchestvo' => $urow['otchestvo'],
		'birthday' => $urow['birthday'],
		'signature' => $urow['signature'],
		'phone' => ((!$_SESSION['logged'] && $urow['phone_only_reg']) ? false : $urow['phone']),
		'rate' => $urow['rate'],
		'rateClass' => $urow['rate'] > 0 ? 'ratingUp' : ($urow['rate'] < 0 ? 'ratingDown' : ''),
		'active' => $urow['active'],
		'url' => '/user/' . $urow['ulogin'],
		'status' => (strtolower($urow['ulogin']) == $_SESSION['ulogin'] ? 'online' : (time() - $urow['last_entry'] * 1 <= 600 ? 'online': 'offline'))
	);
	$row['date'] = ruDate($row['time']) . ' � ' . date('G:i', $row['time']);
	for($i = 0; $i <= 10; $i++) {
		if($row['photo' . ($i + 1)]) {
			$row['photo'][] = array(
					 '/upload/'.md5('photo-' . $row['id'] . '-' . ($i + 1) . ($row['photo' . ($i + 1)] >= 5 ? '-' . $row['photo' . ($i + 1)] : '')).'.jpg',
					 '/upload/r/'.md5('photo-' . $row['id'] . '-' . ($i + 1) . ($row['photo' . ($i + 1)] >= 5 ? '-' . $row['photo' . ($i + 1)] : '')) . '.jpg'
			);
		};
	};
	$row['description'] = preg_replace('/\n/', '<br>', $row['description']);
	$entries[] = $row;
};
$neighbours = 3;
$left_neighbour = $page - $neighbours;
if ($left_neighbour < 1) $left_neighbour = 1;
$right_neighbour = $page + $neighbours;
if ($right_neighbour > $pages) $right_neighbour = $pages;
if ($page > 1) {
	$paginator .= '<a href="/board' . (isset($_GET['category']) ? '/category/' . $_GET['category'] : '') . '/page/' . ($page-1) . '">&larr; �����</a>';
};
for ($i=$left_neighbour; $i<=$right_neighbour; $i++) {
	if ($i != $page) {
		$paginator .= '<a href="/board' . (isset($_GET['category']) ? '/category/' . $_GET['category'] : '') . '/page/' . $i . '">' . $i . '</a>';
	} else {
		$paginator .= '<strong>' . $i . '</strong>';
	};
};
if ($page < $pages) {
	$paginator .= '<a href="/board' . (isset($_GET['category']) ? '/category/' . $_GET['category'] : '') . '/page/' . ($page+1) . '">������ &rarr;</a>';
}

$smarty -> assign('list', array(
	'categories' => $categories,
	'entries' => $entries,
	'pages' => $pages,
	'paginator' => $paginator,
	'action' => $_GET['act'],
	'url' => array(
		'add' => '/board/add'
	)
));

$smarty -> assign('title', $site['options']['static_pages']['list'] . ' - ' . $site['name']);

$smarty -> display('static.tpl');
?>